#### Function agnumerical univariate numerical analyses ########################
### Created: 180312 Author: Alfonso Garmendia  algarsal at upvnet dot upv dot es

### V1.0.1 180614: Name changed, variable also pointed by number,
###                change structure to fit agautoanalyses.
### V1.0.0: 180314 working.
### V0.0.1: output a list with,
###           - title: from parameters
###           - tables: from agdistrplot
###           - captables: create
###           - plots:  from agdistrplot
###           - capplots: create
###           - texts: create

#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
#### Roxigen ####
#' All univariate numerical analyses
#'
#' Plots and tables for numerical variables in a data frame.
#'
#' @param df0 A data frame.
#' @param numvar A character vector with the names of the numerical variables.
#' @param title0 The title of the analysis.
#' @param forcenumeric Logical. If TRUE numvar variables are coerced to numeric.
#'
#' @return
#' A list with tables and plots. Histograms and distributions fit, Tables with
#' details of the fit.
#'
#' @export
#'
#' @examples
#' \dontshow{\donttest{
#' H <- agnumerical(df0, v$num)
#' H$plots
#' }}
#'
#' H <- agnumerical(iris, names(iris)[1:4])
#' H$tables[[1]]
#' H$plots
#'
#' @author Alfonso Garmendia (Universitat Politècnica de València)
#'
#### Function ####
agnumerical <- function(df0, numvar = NULL,
                        title0 = "Numeric variables", forcenumeric = FALSE) {
  #### Requires ####
  # requires agdistrplot() and dependencies

  # df0 must be data frame
  # numvar must be character with names of numeric variables

  #### Input validation ####
  if (TRUE) {
    #### df0 ####
    df0 <- as.data.frame(df0)
    if (!is.data.frame(df0)) {
      stop("agnumerical(): df0 needs to be a data frame. Cannot be coerced.")
    }
    if (length(df0) == 0) {
      stop("agnumerical(): df0 must have at least one variable.")
    }

    #### NULL variables ####
    if (is.null(numvar)) {
      numvar <- 1:length(df0)
    } # all variables are numvar

    #### check variables ####
    numvar <- agcheckvar(df0 = df0, v = numvar, funame = "agnumerical",
                         vname = "numvar", dfname = "df0")

    # df0 <- df0[numvar]

    #### Force as numeric ####
    if (forcenumeric) df0 <- as.data.frame(lapply(df0, as.numeric))

    #### FILTER only numeric variables ####
    nv0 <- numvar
    numvar <-
      numvar[sapply(df0[numvar], FUN = class) %in% c("numeric",  "integer")]

    #### Warning if not numerical in numvar ####
    if (!identical(nv0, numvar)) {
      notnum <- nv0[!nv0 %in% numvar]
      notnumnames <- agvectorastext(names(df0)[notnum])
      sp <- ifelse(length(notnum) == 1, "'. It was ", "'. They were " )
      warning(paste0("Function agnumerical(). Not numeric variable(s) chosen: '",
                     notnumnames, sp,
                     "excluded from analyses."))
    }

    #### Filter data frame ####
    df0 <- df0[numvar]
  }
  #### Init ####
  out <- list()

  ### Title
  out$title <- title0

  #### Tables and plots ####
  # lapply(df0, agdistrplot(xlab = names(df0)))
  # names(df0)
  H <- list()
  # i <- names(df0)[2]
  for (i in names(df0)) {
    H[[i]] <-
      suppressMessages(suppressWarnings(agdistrplot(variable = df0[[i]]
                                                    , xlab = i)))
  }
  #### Arrange output ####
  # i <- names(df0)[1]
  for (i in names(df0)) {
    out[[i]]$tables <- H[[i]]$table
    out[[i]]$captables <-
      paste(
        i,
        "goodness of fit indices and parameter estimates",
        "for different distributions.",
        "KS_p, Kolmogorov–Smirnov test p value;",
        "AIC, Akaike information criterion and",
        "BIC, Bayesian information criterion."
      )
    out[[i]]$plots <- H[[i]]$plot
    out[[i]]$capplots <-
      paste0(
        "Histogram (grey) and density curve (black) of ",
        i,
        ". Estimation of different distributions with",
        " their parameters and p value."
      )
  }
  out$explanation$texts <- H[[1]]$explained

  #### Return ####
  return(out)
}

