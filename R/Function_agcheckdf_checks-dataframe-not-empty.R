###### Function agcheckdf check dataframe is not empty #########################
### Created: 180618 Author: Alfonso Garmendia  algarsal at upvnet dot upv dot es

### V0.0.1: Working version.
#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

#### Roxigen documentation ####
#' Check dataframe object
#'
#' Checks that the object is a data frame and is not empty and if not returns
#' error messages.
#'
#' @param df0 A data frame
#' @param dfname Name of the data frame object. For error messages.
#' @param funame Name of the function. For error messages.
#' @param minvar Minimum number of variables to be an acceptable data frame.
#' @param minobs Minimum number of observations to be an acceptable data frame.
#'
#' @return
#' A non empty data frame in data.frame format or error messages.
#'
#' @export
#'
#' @examples
#' iris <- agcheckdf(df0 = iris, dfname = "Dataframe_name", funame = "Function_name")
#'
#' ### error
#' df1 <- data.frame()
#' df1 <- agcheckdf(df0 = df1, dfname = "df1", funame = "Function_name")
#'
#' ### other error
#' df1 <- 5
#' df1 <- agcheckdf(df0 = df1, dfname = "df1", funame = "Function_name")
#'
#' ### other error
#' df1 <- "a"
#' df1 <- agcheckdf(df0 = df1, dfname = "df1", funame = "Function_name")
#'
#' @seealso
#' \code{\link{agcheckvar}} for checking variables.
#'
#' @author Alfonso Garmendia (Universitat Politècnica de València)
#'
##### Init function ####
# df0 <- iris
# dfname = "df0"
# funame = "agcheckdf"

#### Function ####
agcheckdf <- function(df0 = df0, dfname = "Dataframe_name",
                      funame = "Function_name", minvar = 1, minobs = 3) {
  #### df0 ####
  df0 <- as.data.frame(df0)
  if (!is.data.frame(df0)) {
    stop(paste0(funame, '(): "', dfname, '" needs to be a data frame. ',
                'Could not be coerced.'))
  }
  lv <- length(df0)
  if (lv < minvar) {
    stop(paste0(funame, '(): "', dfname, '" must have at least ', minvar,
                ' variable(s). It has only: ', lv, ' variables.'))
  }
  lo <- length(df0[[1]])
  if (lo < minobs) {
    stop(paste0(funame, '(): "', dfname, '" must have at least ', minobs,
                ' observations. It has only ', lo, ' observations.'))
  }

  return(df0)
}
