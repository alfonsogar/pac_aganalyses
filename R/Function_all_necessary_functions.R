####### ALL FUNCTIONS USED FOR ANALYSES ########################################
### Created: 180305 Author: Alfonso Garmendia  algarsal at upvnet dot upv dot es

### v1.0.2 180611: extracted - agvcharacteristics
### v1.0.1: - aghistogram, - agdistrplot, - agsistema, - agunivarnum
### V1.0.0: Working version:
###                       - agvectorastext
###                       - agvcharacteristics
###                       - agtablenumeric
###                       - agtablecategorical
###                       - agunivar_barplot
###                       - agunivar_categorical
###                       - agdistrtry
#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
#### Function agvectorastext from vector to text comma separated ####
### requires a character vector

#' Vector as text
#'
#' Exports as a comma separated string all the strings in a character vector.
#'
#' @param vector A character vector.
#' @param sep Separator. Default to comma.
#'
#' @return A string
#' @export
#'
#' @examples
#' vector <- LETTERS[1:20]
#' agvectorastext(vector)
#'
#' vector <- LETTERS[1]
#' agvectorastext(vector)
#'
#' #' @author Alfonso Garmendia (Universitat Politècnica de València)
#'
agvectorastext <- function(vector, sep = ", ") {
  vector <- as.character(vector)
  txt <- vector[1]
  if (length(vector) > 1) {
    for (i in 2:length(vector)) {
      txt <- paste(txt, vector[i], sep = sep)
    }
  }
  return(txt)
}

### Example ###
# vector <- LETTERS[1:20]
# agvectorastext(vector)

# vector <- LETTERS[1]
# agvectorastext(vector)


#_______________________________________________________________________________

#### Function agtablenumeric General characteristics of numeric variables ####

#' Numeric variables characteristics
#'
#' Table for characteristics for the numeric variable in a data frame:
#' Number of original and missing data, mean, median and standard deviation
#' for each variable.
#'
#' @param df0 A data frame.
#' @param num_var Character vector with names of numeric variable into df0.
#'
#' @return
#' A data frame with the characteristics for numeric variables.
#'
#' @export
#'
#' @examples
#' agtablenumeric(iris, names(iris)[1:4])
#'
#' @author Alfonso Garmendia (Universitat Politècnica de València)
#'
agtablenumeric <- function(df0, num_var) {
  ### Table of characteristics for numeric variables

  #### Dataframe validation ####
  if (!(is(df0, "data.frame") )) {
    ### tibble is automatically a data frame
    if (is.matrix(df0)) {
      df0 <- as.data.frame(df0)
    } else stop("agtablenumeric() requires df0 to be a data.frame, tibble or matrix as input.")
  }

  #### num_var validation presence and numeric ####
  for (i in 1:length(num_var)) {
    if (!(num_var[i] %in% colnames(df0))) {
      stop(paste0("Target variable ", num_var[i],
                  " not present in dataframe for agtablenumeric()."))
    }
    if (!(all(sapply(df0[num_var], FUN = class) %in% c("numeric", "integer",
                                                       "double")))) {
      txt <- !(sapply(df0[num_var], FUN = class) %in% c("numeric", "integer",
                                                    "double"))
      txt <- agvectorastext(num_var[txt])
      stop(paste0("Not numeric Variable(s) for agtablenumeric(): ", txt))
    }
  }

  #### descriptive table for numeric variables ####
  # var_name <- "Yield"
  univar_num_features_general <- lapply(num_var, function(var_name) {
    val <- data.frame(Variable = var_name,
                      Original = length(df0[[var_name]]),
                      Missing = sum(is.na(df0[[var_name]])),
                      Mean = mean(na.exclude(df0[[var_name]])),
                      Median = median(na.exclude(df0[[var_name]])),
                      SD = sqrt(var(na.exclude(df0[[var_name]])))
    )
    # val$Final <- val$Original - val$Missing
    n <- val$Original - val$Missing
    val <- cbind(val[1], n, val[-1])
    val[-1] = lapply(val[-1], function(x) round(x, 2))

    return(val)
  })
  table <- data.frame()
  for (i in 1:length(univar_num_features_general)) {
  table <- rbind(table, univar_num_features_general[[i]][1,])
  }
  return(table)
}

### EXAMPLES ###

# agtablenumeric(iris, names(iris)[1:4])
#_______________________________________________________________________________

#### Function agtablecategorical General for categorical variables #############
# df0 <- Tree
# cat_var <- res$vars$vncategoricalall
# excluded <- res$vars$vnexcluded

#' Categorical variables characteristics
#'
#' Table of characteristics for categorical variables, indicating if excluded.
#'
#' @param df0 A data frame.
#' @param cat_var Character vector with names of categorical variables in df0.
#' @param excluded Character vector with excluded categorical variables in df0.
#'
#' @return
#' A data frame.
#'
#' @export
#'
#' @examples
#' agtablecategorical(iris, names(iris)) # Error
#' agtablecategorical(as.data.frame(Titanic),
#'                    names(as.data.frame(Titanic))[1:4])
#'
#' @author Alfonso Garmendia (Universitat Politècnica de València)
#'
agtablecategorical <- function(df0, cat_var, excluded = NULL) {
  ### Table of characteristics for categorical variables, indicating if excluded

  #### Dataframe validation ####
  if (!(is(df0, "data.frame") )) {
    ### tibble is automatically a data frame
    if (is.matrix(df0)) {
      df0 <- as.data.frame(df0)
    } else stop("agtablecategorical() requires df0 to be a data.frame, tibble or matrix as input.")
  }

  df0 <- df0[cat_var]
  # df0 <- as.data.frame(df0)

  #### cat_var validation presence and numeric ###
  # i = 1
  for (i in 1:length(cat_var)) {
    if (!(cat_var[i] %in% colnames(df0))) {
      stop(paste0("Target variable ", cat_var[i],
                  " not present in dataframe for agtablecategorical()."))
    }
    if (!(all(sapply(df0, FUN = class) %in% c("character", "factor", "logical")))) {
      txt <- !(sapply(df0, FUN = class) %in% c("character", "factor", "logical"))
      txt <- agvectorastext(cat_var[txt])
      stop(paste0("Not categorical Variable(s) for agtablecategorical(): ", txt))
    }
  if (is.logical(df0[[cat_var[i]]])) df0[cat_var][i] <- as.factor(df0[[cat_var[i]]])
  if (is.character(df0[[cat_var[i]]])) df0[cat_var][i] <- as.factor(df0[[cat_var[i]]])
  }

  #### descriptive table for categorical variables ####
  # var_name <- "Yield"
  univar_cat_features_general <- lapply(cat_var, function(var_name) {
    val <- data.frame(Variable = var_name,
                      Original = length(df0[[var_name]]),
                      Missing = sum(is.na(df0[[var_name]])),
                      Levels = length(levels(df0[[var_name]]))
                      )
    # val$Final <- val$Original - val$Missing
    n <- val$Original - val$Missing
    val <- cbind(val[1], n, val[-1])
    val[-1] = lapply(val[-1], function(x) round(x, 2))

    return(val)
  })
  table <- data.frame()
  for (i in 1:length(univar_cat_features_general)) {
    table <- rbind(table, univar_cat_features_general[[i]][1,])
  }
  table$Analysis = ifelse(table$Variable %in% excluded,
                          "Excluded",
                          "Analyzed")
  return(table)
}

### EXAMPLES ###

# agtablecategorical(iris, names(iris)) # Error
# agtablecategorical(as.data.frame(Titanic), names(as.data.frame(Titanic))[1:4])

# agtablecategorical(Tree, res$vars$vncategoricalall, excluded = res$vars$vnexcluded)

#_______________________________________________________________________________

#### Function agdistrtry check possible distributions ##########################
### Created: 180312 Author: Alfonso Garmendia  algarsal at upvnet dot upv dot es

### V0.0.1: Fork from HistDistr. Try different distributions and
###         returns a character vector with the possible distributions names
#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
# variable <- iris$Sepal.Length

#' Try distributions.
#'
#' Try different distributions and returns a character vector with the possible
#'  distributions names. It avoid errors in other functions because of trying
#'  not possible distributions.
#'
#' @param variable Variable to check distributions.
#' @param num_samples Number of samples.
#' @param distributions Distributions to try.
#'
#' @return
#' A character vector with the names of the distributions.
#'
#' @import MASS
#' @import fitdistrplus
#'
#' @export
#'
#' @examples
#' H <- trydistr(iris[[4]])
#' H <- suppressWarnings(trydistr(iris[[1]]))
#' H
#'
#' @author Alfonso Garmendia (Universitat Politècnica de València)
#'
agdistrtry <- function(variable
                       , num_samples = 100000
                       , distributions = c("normal"
                                           , "logistic"
                                           , "negative binomial"
                                           , "poisson"
                                           , "exponential"
                                           , "geometric"
                       )
) {
  #### Description ####
  ### variable: Numeric variable
  ### num_samples: Number of samples for distributions simulations
  ### distributions: Distributions to try

  #### Libraries ####
  # require(MASS)
  # require(fitdistrplus)

  rtn <- character()

  ### DISTRIBUTIONS ####
  options(show.error.messages = FALSE)
  options(warn = FALSE)

  for (i in 1:length(distributions)) {

    #### Normal distribution ####
    # i = 1
    if (distributions[i] == "normal") {
      ### Try fit
      fit <- class(try(fitdistr(variable, distributions[i]), silent = TRUE))
      fit2 <- class(try(fitdist(variable, "norm"), silent = TRUE))
      ### check
      if (!any(fit == "try-error", fit2 == "try-error")) {
        rtn <- c(rtn, distributions[i])
      }
    }

    #### Poisson distribution ####
    # i = 3
    if (distributions[i] == "poisson") {
      ### Try fit
      fit <- class(try(fitdistr(variable, distributions[i]), silent = TRUE))
      fit2 <- class(try(fitdist(variable, "pois"), silent = TRUE))
      ### check
      if (!any(fit == "try-error", fit2 == "try-error")) {
        rtn <- c(rtn, distributions[i])
      }
    }

    #### Negative binomial ####
    # i = 4
    if (distributions[i] == "negative binomial") {
      ### Try fit
      fit <- class(try(fitdistr(variable, distributions[i]), silent = TRUE))
      fit2 <- class(try(fitdist(variable, "nbinom"), silent = TRUE))
      ### check
      if (!any(fit == "try-error", fit2 == "try-error")) {
        rtn <- c(rtn, distributions[i])
      }
    }

    #### Exponential ####
    # i = 5
    if (distributions[i] == "exponential") {
      ### Try fit
      fit <- class(try(fitdistr(variable, distributions[i]), silent = TRUE))
      fit2 <- class(try(fitdist(variable, "exp"), silent = TRUE))
      ### check
      if (!any(fit == "try-error", fit2 == "try-error")) {
        rtn <- c(rtn, distributions[i])
      }
    }

    #### Geometric ####
    # i = 6
    if (distributions[i] == "geometric") {
      ### Try fit
      fit <- class(try(fitdistr(variable, distributions[i]), silent = TRUE))
      fit2 <- class(try(fitdist(variable, "geom"), silent = TRUE))
      ### check
      if (!any(fit == "try-error", fit2 == "try-error")) {
        rtn <- c(rtn, distributions[i])
      }
    }

    #### Logistic ####
    # i = 2
    if (distributions[i] == "logistic") {
      ### Try fit
      fit <- class(try(fitdistr(variable, distributions[i]), silent = TRUE))
      fit2 <- class(try(fitdist(variable, "logis"), silent = TRUE))
      ### check
      if (!any(fit == "try-error", fit2 == "try-error")) {
        rtn <- c(rtn, distributions[i])
      }
    }
  }
  options(show.error.messages = TRUE)
  options(warn = TRUE)

  ### return
  return(rtn)
}

#### EXAMPLE ###
# H <- trydistr(iris[[4]])
# H <- suppressWarnings(trydistr(iris[[1]]))
# H

#### Function aghistogram plots ggplot histogram with density ################
### Created: 180312 Author: Alfonso Garmendia  algarsal at upvnet dot upv dot es

### V0.0.1: plot histogram and density together
### V0.0.2 180716: fix ggtheme
#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
# variable <- iris$Sepal.Length[1:10]
# xlab <- "Sepal length"

#' Plot histogram and density
#'
#' Plots histogram and density together in the same plot.
#'
#' @param variable String pointing to numeric variable.
#' @param xlab Variable name in the plot.
#' @param ggtheme Plot theme.
#' @param bin Bin parameter. Default = 20.
#' @param title Title of the plot.
#'
#' @return
#' A ggplot
#'
#' @import ggplot2
#'
#' @export
#'
#' @examples
#' aghistogram(iris$Sepal.Length, "Sepal length in cm")
#'
#' @seealso
#' \url{https://stackoverflow.com/a/5688125}
#'
#' @author Alfonso Garmendia (Universitat Politècnica de València)
#'
#'
aghistogram <- function(variable, xlab = "Variable",
                        ggtheme = ggplot2::theme_bw(),
                        bin = 20, title = TRUE) {
  ### help here: https://stackoverflow.com/a/5688125
  ### variable numeric variable
  ### xlab: text for x label

  ### requires
  # require(ggplot2)

  ### adjust bin
  if (bin == 20) {
    if (bin > 1 + length(variable) / 3) {
      bin = round(length(variable) / 3)
      warning(paste("Parameter 'bin = 20' too large. Changed to:", bin))
    }
  }
  ### needed data frame
  df0 = data.frame(na.exclude(variable))
  names(df0) <- xlab

  ### plot
  g <- ggplot(data = df0) +
    geom_histogram(aes(x = variable, y = ..density..),
                   alpha = 0.4, bins = bin) +
    xlab(xlab) +
    geom_line(aes(x = variable, y = ..density.., colour = 'Empirical'),
              stat = 'density') +
    # stat_function(fun = dnorm, aes(colour = 'Normal')) +
    scale_colour_manual(name = 'Density', values = "black")

  g <- g + ggtheme
  # g <- eval(parse(text = paste("g + ",ggtheme)))

  if (title == TRUE) g <- g + ggtitle(paste(g$labels$y, " of ", g$labels$x))

  return(g)
}
#### EXAMPLE ###
# aghistogram(iris$Sepal.Length, "Sepal length in cm")

#### Function agdistrplot plot histogram and selected distributions ##########
### Created: 180312 Author: Alfonso Garmendia  algarsal at upvnet dot upv dot es

### V0.0.1: Fork from HistDistr. plot histogram with selected distributions
### V0.0.2: Fix ggtheme
#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
# variable <- iris$Sepal.Length
# xlab <- "Sepal length"
# ggtheme <- ggplot2::theme_bw()

#' Plot histogram and selected distributions
#'
#' Plot histogram and selected distributions. Distributions are selected using
#' agdistrtry().
#'
#' @param variable String pointing to variable.
#' @param xlab Variable name in plot.
#' @param num_samples Number of samples. Default = 100000.
#' @param distributions Distributions to plot.
#'                      Default = suppressWarnings(agdistrtry(variable))
#' @param col Colours for the different distributions.
#' @param ggtheme Changes plot theme.
#'
#' @return
#' A list with:
#'
#' - A plot.
#'
#' - A table.
#'
#' - An explanatory text.
#'
#' @import MASS
#' @import fitdistrplus
#'
#' @export
#'
#' @examples
#' H <- histdistr(variable, xlab = "Yield")
#' H <- histdistr(iris[[4]], xlab = "Petal.Width")
#' H <- suppressWarnings(histdistr(iris[[4]], xlab = "Petal.Width"))
#' H <- suppressMessages(suppressWarnings(histdistr(iris[[4]], xlab = "Petal.Width")))
#' H$plot
#' H$table
#' H$explained
#'
#' @author Alfonso Garmendia (Universitat Politècnica de València)
#'
agdistrplot <- function(variable
                      , xlab = "Variable"
                      , num_samples = 100000
                      , distributions = suppressWarnings(agdistrtry(variable))
                      , col = c("red", "green", "blue"
                                , "orange", "salmon", "cyan"
                                , "purple", "seagreen"),
                      ggtheme = ggplot2::theme_bw()
) {
  #### Libraries ####
  # require(MASS)          # for fitdistr()
  # require(fitdistrplus)  # for chi-square, AIK, BIC
  # requires agdistrtry() to try the distributions and avoid errors
  # requires aghistogram()

  #### EXPLANATION ####
  e <- paste("Kolmogorov–Smirnov test (KS_p): Is simple nonparametric test",
             "for one dimensional probability distribution.
             ",
             "Same as Cramer von Mises test, ",
             "it compares empirical distribution with the reference probability",
             "distribution with the estimated paremeters.
             ",
             "For goodness of fit we have the following hypothesis:
             H0 = The variable is consistent with a specified reference distribution.
             H1 = The variable is NOT consistent with the reference distribution

             LogLikelihood, AIC and BIC indexes are indexes for likelihood.",
             "The lower the value, the better the fit."
  )

  #### List for return ####
  LS <- list()
  LS$df <- data.frame(Distribution = NA
                      , KS_p = NA
                      , LogLikelihood = NA
                      , AIC = NA
                      , BIC = NA
                      , param1_n = NA
                      , param1 = NA
                      , param2_n = NA
                      , param2 = NA
  )
  # legends <- xlab
  # colores <- col[1]

  ### DISTRIBUTIONS ####
  variable <- as.numeric(na.exclude(variable))
  # i = 1
  for (i in 1:length(distributions)) {

    ### for all distributions
    x <- seq(min(variable), max(variable), by = 0.01)

    #### Normal distribution ####
    # i = 1
    if (distributions[i] == "normal") {

      ### Fit
      fit <- fitdistr(variable, distributions[i])
      fit2 <- fitdist(variable, "norm")
      y <- rnorm(n = num_samples
                 , mean = fit$estimate[[1]]
                 , sd = fit$estimate[[2]])

      ### Kolmogorov Smirnov Test
      result = ks.test(variable, y)

      ### Datos
      LS$df[i,1] <- distributions[i]
      LS$df[i,2] <- result$p.value
      LS$df[i,3] <- fit$loglik
      LS$df[i,4] <- fit2$aic
      LS$df[i,5] <- fit2$bic
      LS$df[i,6] <- names(fit$estimate)[1]
      LS$df[i,7] <- fit$estimate[[1]]
      LS$df[i,8] <- names(fit$estimate)[2]
      LS$df[i,9] <- fit$estimate[[2]]

      ### Legend for this density
      LS$dis$normal$legend <- paste(distributions[i]
                                    , "("
                                    , format(fit$estimate[[1]]
                                             , digits = 2)
                                    , ", "
                                    , format(fit$estimate[[2]]
                                             , digits = 2)
                                    , "). p = "
                                    , format(result$p.value
                                             , digits = 2)
                                    , sep = "")

      ### Density line
      LS$dis$normal$df <- data.frame(x = x,
                                     y = dnorm(x,
                                               mean = fit$estimate[[1]],
                                               sd = fit$estimate[[2]])
      )
      LS$dis$normal$col <- col[i]
      # g + geom_line(data = LS$dis$normal$df, aes(x = x, y = y,
      #                                       colour = LS$dis$normal$legend)) +
      #   scale_colour_manual(name = 'Density', values = c("black", "red"))
    }

    #### Poisson distribution ####
    # i = 3
    if (distributions[i] == "poisson") {

      ### Fit
      fit <- fitdistr(variable, distributions[i])
      fit2 <- fitdist(variable, "pois")
      y <- rpois(n = num_samples
                 , lambda = fit$estimate[[1]])

      ### Kolmogorov Smirnov Test
      result = ks.test(variable, y)

      ### Datos
      LS$df[i,1] <- distributions[i]
      LS$df[i,2] <- result$p.value
      LS$df[i,3] <- fit$loglik
      LS$df[i,4] <- fit2$aic
      LS$df[i,5] <- fit2$bic
      LS$df[i,6] <- names(fit$estimate)[1]
      LS$df[i,7] <- fit$estimate[[1]]

      ### Legend for this density
      LS$dis$poisson$legend  <- paste(distributions[i]
                                      , "("
                                      , format(fit$estimate[[1]]
                                               , digits = 2)
                                      , "). p = "
                                      , format(result$p.value
                                               , digits = 2)
                                      , sep = "")

      ### Density line
      LS$dis$poisson$df <- data.frame(x = x,
                                      y = dpois(x,
                                                lambda = fit$estimate[[1]]) )
      LS$dis$poisson$col <- col[i]
      # g + geom_line(data = LS$dis$poisson$df, aes(x = x, y = y,
      #                                       colour = LS$dis$poisson$legend)) +
      #   scale_colour_manual(name = 'Density', values = c("black", "red"))
    }

    #### Negative binomial ####
    # i = 4
    if (distributions[i] == "negative binomial") {

      ### Fit
      fit <- fitdistr(variable, distributions[i])
      fit2 <- fitdist(variable, "nbinom")

      # rnbinom(n, size, prob, mu)
      y <- rnegbin(n = num_samples
                   , theta = fit$estimate[[1]]
                   , mu = fit$estimate[[2]])
      #plot(density(y))

      ### Kolmogorov Smirnov Test
      result = ks.test(variable, y)

      ### Datos
      LS$df[i,1] <- distributions[i]
      LS$df[i,2] <- result$p.value
      LS$df[i,3] <- fit$loglik
      LS$df[i,4] <- fit2$aic
      LS$df[i,5] <- fit2$bic
      LS$df[i,6] <- names(fit$estimate)[2]
      LS$df[i,7] <- fit$estimate[[2]]
      LS$df[i,8] <- names(fit$estimate)[1]
      LS$df[i,9] <- fit$estimate[[1]]

      ### Legend for this density
      LS$dis$nbin$legend <- paste(distributions[i]
                                  , "("
                                  , format(fit$estimate[[2]]
                                           , digits = 2)
                                  , ", "
                                  , format(fit$estimate[[1]]
                                           , digits = 2)
                                  , "). p = "
                                  , format(result$p.value
                                           , digits = 2)
                                  , sep = "")

      ### Density line

      LS$dis$nbin$df <- data.frame(x = x,
                                   y = dnbinom(x,
                                               size = fit$estimate[[1]], # theta
                                               mu = fit$estimate[[2]]) )
      LS$dis$nbin$col <- col[i]

      # g + geom_line(data = LS$dis$nbin$df, aes(x = x, y = y,
      #                                       colour = LS$dis$nbin$legend)) +
      #   scale_colour_manual(name = 'Density', values = c("black", "red"))

    }

    #### Exponential ####
    # i = 5
    if (distributions[i] == "exponential") {

      ### Fit
      fit <- fitdistr(variable, distributions[i])
      fit2 <- fitdist(variable, "exp")

      y <- rexp(n = num_samples
                , rate = fit$estimate[[1]])

      ### Kolmogorov Smirnov Test
      result = ks.test(variable, y)

      ### Datos
      LS$df[i,1] <- distributions[i]
      LS$df[i,2] <- result$p.value
      LS$df[i,3] <- fit$loglik
      LS$df[i,4] <- fit2$aic
      LS$df[i,5] <- fit2$bic
      LS$df[i,6] <- names(fit$estimate)[1]
      LS$df[i,7] <- fit$estimate[[1]]

      ### Legend for this density
      LS$dis$exponential$legend <- paste(distributions[i]
                                         , "("
                                         , format(fit$estimate[[1]]
                                                  , digits = 2)
                                         , "). p = "
                                         , format(result$p.value
                                                  , digits = 2)
                                         , sep = ""
      )

      ### Density line
      LS$dis$exponential$df <- data.frame(x = x,
                                          y = dexp(x,
                                                   rate = fit$estimate[[1]]) )
      LS$dis$exponential$col <- col[i]

      # g + geom_line(data = LS$dis$exponential$df, aes(x = x, y = y,
      #                                       colour = LS$dis$exponential$legend)) +
      #   scale_colour_manual(name = 'Density', values = c("black", "red"))

    }

    #### Geometric ####
    # i = 6
    if (distributions[i] == "geometric") {

      ### Fit
      fit <- fitdistr(variable, distributions[i])
      fit2 <- fitdist(variable, "geom")

      y <- rgeom(n = num_samples
                 , prob = fit$estimate[[1]]
      )


      ### Kolmogorov Smirnov Test
      result = ks.test(variable, y)

      ### Datos
      LS$df[i,1] <- distributions[i]
      LS$df[i,2] <- result$p.value
      LS$df[i,3] <- fit$loglik
      LS$df[i,4] <- fit2$aic
      LS$df[i,5] <- fit2$bic
      LS$df[i,6] <- names(fit$estimate)[1]
      LS$df[i,7] <- fit$estimate[[1]]

      ### Legend for this density
      LS$dis$geom$legend <- paste(distributions[i]
                                  , "("
                                  , format(fit$estimate[[1]]
                                           , digits = 2)
                                  , "). p = "
                                  , format(result$p.value
                                           , digits = 2)
                                  , sep = ""
      )
      ### Density line
      LS$dis$geom$df <- data.frame(x = x,
                                   y = dgeom(x,
                                             prob = fit$estimate[[1]]) )
      LS$dis$geom$col <- col[i]

      # g + geom_line(data = LS$dis$geom$df, aes(x = x, y = y,
      #                                       colour = LS$dis$geom$legend)) +
      #   scale_colour_manual(name = 'Density', values = c("black", "red"))

    }
    #### Logistic ####
    # distributions[2]
    # i = 2
    if (distributions[i] == "logistic") {

      ### Fit
      fit <- fitdistr(variable, distributions[i])
      fit2 <- fitdist(variable, "logis")

      y <- rlogis(n = num_samples
                  , location = fit$estimate[[1]]
                  , scale = fit$estimate[[2]]
      )

      ### Kolmogorov Smirnov Test
      result = ks.test(variable, y)

      ### Datos
      LS$df[i,1] <- distributions[i]
      LS$df[i,2] <- result$p.value
      LS$df[i,3] <- fit$loglik
      LS$df[i,4] <- fit2$aic
      LS$df[i,5] <- fit2$bic
      LS$df[i,6] <- names(fit$estimate)[1]
      LS$df[i,7] <- fit$estimate[[1]]
      LS$df[i,8] <- names(fit$estimate)[2]
      LS$df[i,9] <- fit$estimate[[2]]

      ### Legend for this density
      LS$dis$logis$legend  <- paste(distributions[i]
                                    , "("
                                    , format(fit$estimate[[1]]
                                             , digits = 2)
                                    , ", "
                                    , format(fit$estimate[[2]]
                                             , digits = 2)
                                    , "). p = "
                                    , format(result$p.value
                                             , digits = 2)
                                    , sep = ""
      )

      ### Density line
      LS$dis$logis$df <- data.frame(x = x,
                                    y = dlogis(x,
                                               location = fit$estimate[[1]]
                                               , scale = fit$estimate[[2]]
                                    ) )
      LS$dis$logis$col <- col[i]

      # g + geom_line(data = LS$dis$logis$df, aes(x = x, y = y,
      #                                       colour = LS$dis$logis$legend)) +
      #   scale_colour_manual(name = 'Density', values = c("black", "red"))
    }

  }
  #### PLOT ####
  ### Histogram ####
  scalecolors <- c("black", LS$dis[[1]]$col)
  scalebreaks <- c(LS$dis[[1]]$legend)
  #
  df1 <- LS$dis[[1]]$df
  df1$f <- rep("z1", length(df1$x))

  if (length(LS$dis) > 1) {
    for (i in  2:length(LS$dis)) {
      df2 <- LS$dis[[i]]$df
      df2$f <- rep(paste0("z", i), length(df2$x))

      df1 <- rbind(df1, df2)
      rm(df2)

      scalecolors <- c(scalecolors, LS$dis[[i]]$col)
      scalebreaks <- c(scalebreaks, LS$dis[[i]]$legend)
    } }

  g <- aghistogram(variable, xlab, ggtheme = ggtheme)

  g <- g + geom_line(data = df1, aes(x = x, y = y, colour = factor(f))) +
    scale_colour_manual(name = "Density",
                        values = scalecolors,
                        labels = c("Empirical", scalebreaks))

  #### return ####
  L <- list()
  L$plot <- g
  L$table <- LS$df
  L$explained <- e
  return(L)
}

#### EXAMPLE ###
# H <- histdistr(variable, xlab = "Yield")
# H <- histdistr(iris[[4]], xlab = "Petal.Width")
# H <- suppressWarnings(histdistr(iris[[4]], xlab = "Petal.Width"))
# H <- suppressMessages(suppressWarnings(histdistr(iris[[4]], xlab = "Petal.Width")))
# H$plot
# H$table
# H$explained

#### Function agsistema SYSTEM DETAILS ###########################################
### Created: 171019 Author: Alfonso Garmendia  algarsal at upvnet dot upv dot es

### V1.0.1 171219: Translat to English
### V1.0.0 171216: clean an working
###                change plyr::join to dplyr::left_join(pac,ip)
#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
### System characteristics when creating document

#' System characteristics
#'
#' A summary of the system characteristics when creating document.
#'
#' @return
#' A summary text
#'
#' @export
#'
#' @examples
#' sistema()
#'
#' @author Alfonso Garmendia (Universitat Politècnica de València)
#'
agsistema <- function() {
  kable(data.frame(SYSTEM = c(R.version$version.string
                              , R.version$nickname
                              , R.version$platform),stringsAsFactors = FALSE)
  )
  # R.version$version.string
  # R.version$nickname
  # R.version$platform
  #
  ip <- as.data.frame(installed.packages()[,c(1,3:4)])
  rownames(ip) <- NULL
  #ip <- ip[is.na(ip$Priority),1:2,drop = FALSE]
  ip2 <- paste(ip$Package, ip$Version)
  # print(ip2)

  pac <- data.frame(Package = .packages(), stringsAsFactors = FALSE)
  ip$Package <- as.character(ip$Package)
  pac <- suppressMessages(dplyr::left_join(pac,ip))
  # pac <- suppressMessages(plyr::join(pac, ip))
  pac <- pac[order(pac$Package),]
  pac <- pac[order(pac$Priority),]
  rownames(pac) <- NULL
  kable(pac)

  pac2 <- pac
  pac2$Priority <- addNA(pac2$Priority)
  levels(pac2$Priority)[length(levels(pac2$Priority))] <- "OTHERS"
  pac2$PV <- paste(pac2$Package,pac2$Version, sep = "_")

  a <- split(pac2$PV,pac2$Priority)
  a$base <- split(pac$Package,pac$Priority)$base
  # pac2 <- split(pac2,pac2$Priority)
  # pac2$base <-
  #############
  print(paste(
    R.version$version.string
    , R.version$nickname
    , R.version$platform
  ))
  print(a)

}

####   ####




