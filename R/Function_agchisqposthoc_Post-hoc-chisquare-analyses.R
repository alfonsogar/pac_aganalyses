####### Function agchisqposthoc Chi square analysis and plot ###################
### Created: 180430 Author: Alfonso Garmendia  algarsal at upvnet dot upv dot es

### v0.0.2 180512: Add roxygen2 documentation.
### V0.0.1: Copied from fifer::chisq.post.hoc(), in an attempt to extract
###         the letters automatically. Not working yet.
#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

#### Roxigen ####
#' Post-Hoc chi-square analysis
#'
#' STILL NOT WORKING.
#'
#' This is an attempt to extract the letters automatically
#' from a chi-square test.
#' The idea is using fifer::chisq.post.hoc() for the post hoc ps and
#' the agricolae tukey to extract the letters from these ps.
#'
#' @param tbl A table object.
#' @param test What sort of test will be used?
#'     This must have an object called p.value so it can correct the p-values.
#'     Defaults to "fisher.test"
#' @param popsInRows A logical indicating whether the populations form the
#'     rows (default; =TRUE) of the table or not (=FALSE).
#' @param control A string indicating the method of control to use. See details.
#' @param alpha0 Significance level for groups in post hoc analyses.
#'     Default = 0.05.
#' @param digits A numeric that controls the number of digits to print.
#' @param ... Other arguments sent to whatever test the user specifies.
#'
#' @return
#' Letters for posthoc analysis.
#'
#' @export
#'
#' @examples
#'
#' @details
#'
#'
#' @seealso
#' \code{\link{fifer::chisq.post.hoc}}
#'
#' @author Alfonso Garmendia (Universitat Politècnica de València)
#'

#### FUNCTION ####
agchisqposthoc <-
  function(tbl, test = c("fisher.test"), popsInRows = TRUE,
            control = c("fdr", "BH", "BY", "bonferroni", "holm",
                        "hochberg", "hommel"),
            alpha0 = 0.05, digits = 4, ...)
  {
    #### check variables ####
    control <- match.arg(control)
    test = match.fun(test)
    if (!popsInRows)
      tbl <- t(tbl)
    popsNames <- rownames(tbl)
    prs <- combn(1:nrow(tbl), 2)
    tests <- ncol(prs)
    pvals <- numeric(tests)
    # lbls <-
      var1 <- var2 <- character(tests)
    # i = 1
    for (i in 1:tests) {
      pvals[i] <- test(tbl[prs[, i], ], ...)$p.value
      # lbls[i] <- paste(popsNames[prs[, i]], collapse = " vs. ")
      var1[i] <- popsNames[prs[, i]][1]
      var2[i] <- popsNames[prs[, i]][2]
    }
    adj.pvals <- p.adjust(pvals, method = control)
    Significant <- adj.pvals < alpha0

    cat("Adjusted p-values used the", control, "method.\n\n")
    data.frame(var1 = var1, var2 = var2,
               raw.p = format(pvals, digits = digits),
               adj.p = format(adj.pvals, digits = digits),
               Significant = Significant)
  }

