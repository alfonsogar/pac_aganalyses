####### Function agcheckvar check and convert variables names to numbers  ######
### Created: 180606 Author: Alfonso Garmendia  algarsal at upvnet dot upv dot es

### V0.0.1: Working version.
#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
#### Roxigen ####
#' Check variables names into other functions.
#'
#' Checks if the chosen variable(s) pointed by name(s) or number(s) exist.
#'
#' If by names, checks that names exist into the data frame and converts them
#' to numbers.
#'
#' If by numbers, check that they are lower than the number of
#' variables in the data frame.
#'
#' @param df0 Data frame
#' @param v A number or string (or vector with numbers or names) referring
#'        to dependent variable(s) or target variables.
#' @param ... Names needed for next parameters.
#' @param funame Name of the function. Needed to point error messages.
#'        Default to "agcheckbar".
#' @param vaname Name of the variable. Needed to point error messages.
#'        Default to "v".
#' @param dfname Name of the data frame. Needed to point error messages.
#'        Default to "df0".
#'
#' @return
#' A numeric vector with the reference(s) to variables in data frame *df0*.
#' Or error if something is not well referenced. Errors may have the name of
#' the main function if included in *funame*.
#'
#' @export
#'
#' @examples
#'
#' agcheckvar(df0 = iris, v = c(1,5))
#' agcheckvar(iris, 6) # error
#' agcheckvar(iris, "species", funame = "example_function_name",
#'            vname = "example_variable",
#'            dfname = "example_dataframe") # error
#' agcheckvar(iris, c("Species",  "Petal.Length", "Sepal.Width"))
#'
#' @author Alfonso Garmendia (Universitat Politècnica de València)
#'
#### Init ####
# df0 <- iris
# v <- c("epal", "Species", 1, 2)
# funame = "example_function_name"
# vname = "example_variable"
# dfname = "example_dataframe"

#### Function ####
agcheckvar <- function(df0, v, ..., funame = "agcheckvar",
                       vname = "the variable", dfname = "the dataframe") {
  #### check df0
  df0 <- agcheckdf(df0, dfname = "df0", funame = "agcheckvar")

  ### several variables
  excluded <- numeric()
  for (i in 1:length(v)) {
    x <- v[i]
    if (!is.na(suppressWarnings(as.numeric(x)))) x <- as.numeric(x)
    #### character references to variables ####
    if (is.character(x)) {
      if (!x %in% names(df0)) {
        warning(paste0(funame, '(): Var ', i, ' = "', x, '" in "', vname,
                       '" do not match with any variable name in "', dfname,
                       '". Analyses keep going with the remaining',
                       ' variable(s).'))
        excluded <- c(excluded, i)
      }
      ### Convert to numeric ####
      if (x %in% names(df0)) v[i] <- which(names(df0) %in% x)
    }
    #### numeric references to variables ####
    if (is.numeric(x)) {
      #### check v isnt larger than length dataframe ####
      if (x > length(df0)) {
        warning(paste0(funame, '(): One variable reference in "', vname,
                       '" is larger ', 'than the number of variables in "',
                       dfname, '".',
                       ' Var ', i, ' = ', v[i], ' do not match.',
                    ' Analyses keep going with the remaining variable(s).'))
        excluded <- c(excluded, i)
      }
    }
  }
  #### Exclude wrong variables ####
  if (length(excluded) > 0) v <- v[-excluded]
  if (length(v) == 0) {
    stop(paste0(funame, '(): No variable reference in "', vname,
               '" match with the variables in df0. See warnings().'))
  }

  #### check v numeric integer ####
  v <- as.integer(v)
  # v <- c(1, 3, "Sepal.Length")
  if (!is.integer(v)) {
    stop(paste0(funame, "(): ", vname, " is not right. It should be ",
                "existing variable name(s) or number(s) into ", dfname,
                "."))
  }

  return(v)
}
