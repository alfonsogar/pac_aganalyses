#### Function agvcharacteristics List of variables charact. on data frame ######
### Created: 180305 Author: Alfonso Garmendia  algarsal at upvnet dot upv dot es

### v0.0.3 180504: Add roxygen2 documentation for package
### V0.0.2 180417: Working function univariate and anovas
### V0.0.1: Not working yet. Import data. Some analyses. Some documents
#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
##### Roxigen ####
#' General characteristics of the variables of a data frame
#'
#' Returns a list of characteristics for the data frame df0. How many variables
#' are categorical and how many numerical. Also tables with captions for
#' the descriptive characteristics of the variables.
#'
#' All factors (or character) with more levels than the maximum
#' (max_levels_cat_var), will be excluded from subsequents analyses.
#'
#' @param df0 A data frame
#' @param target_var Dependent or target variable
#' @param df_name Name of the data frame
#' @param ... Names needed for next parameters.
#' @param author For subtitles.
#' @param max_levels_cat_var Maximum number of levels for a categorical variable
#'     to be considered for the analyses.
#' @param top_k_features Not used. How many variables to analyze.
#' @param f_screen_model Not used. Analysis to check the k best variables.
#'
#' @return
#' A list with tables (with captions) and characteristics from a data frame.
#'
#' @export
#'
#' @examples
#' v <- agvcharacteristics(iris, "Species")
#' v$vars$vncategorical
#' v$vars$vntarget
#' iris[[v$vars$vntarget]]
#' iris[v$vars$vntarget]
#'
#' v <- agvcharacteristics(iris, c("Species", "Petal.Width"))
#' v$vars$vntarget
#' iris[[v$vars$vntarget]]  # Not working
#' iris[v$vars$vntarget]
#' iris[[v$vars$vntarget[1]]]
#'
#' @author Alfonso Garmendia (Universitat Politècnica de València)
#'
agvcharacteristics <- function(df0, target_var, df_name = "dataframe", ... ,
                               author = "Alfonso Garmendia (algarsal@upvnet.upv.es)",
                               max_levels_cat_var = 9) {
  ### Returns a list of characteristics for the data frame df0
  ### target var should be character

  #### Dataframe validation ####
  if (!(is(df0, "data.frame") )) {
    ### tibble is automatically a data frame
    if (is.matrix(df0)) {
      df0 <- as.data.frame(df0)
    } else stop(paste("agvcharacteristics() requires df0 to be a data.frame,",
                      "tibble or matrix as input."))
  }

  # #### target_var validation ####
  # for (i in 1:length(target_var)) {
  #   if (!(target_var[i] %in% colnames(df0))) {
  #     stop(paste0("Target variable ", target_var[i],
  #                 " not present in dataframe for agvcharacteristics()."))
  #   }
  # }

  #### try to unquote arguments ####
  # arguments <- as.list(match.call())
  # v$vars$target_var <- arguments$target_var
  # v$vars$target_var <- c(arguments$target_var)

  #### as factor all character ####
  df0[, which(sapply(df0, class) == "character")] = lapply(df0[,
                                                               which(sapply(df0, class) == "character"), drop = F], as.factor)

  #### initialize list ####
  v <- list()

  v$cleandata <- NA                          # initialize to maintain order

  ### title
  v$vars$maintitle <- paste("Autoanalyses for", df_name, "dataframe on",
                            target_var, "dependent variable"
  )
  v$vars$mainsubtitle <- paste("by", author, "on" ,
                               format(Sys.time(), "%Y-%m-%d")
  )

  #### vars number of observations ####
  v$vars$hmobservations <- length(df0[[1]])

  #### vars Variable classes and names ####
  v$vars$data_type <- sapply(df0, class)
  v$vars$vnames <- names(df0)
  v$vars$vntarget <- as.character(target_var)
  # v$vars$vnnumeric <- names(which(sapply(df0, class) == "numeric"))
  v$vars$vnnumeric <- names(v$vars$data_type)[sapply(v$vars$data_type,
                                                     function(x) any(x %in% c("numeric",
                                                                              "integer",
                                                                              "double")))]
  # v$vars$vncategorical <- names(which(sapply(df0, class) == "factor"))
  v$vars$vncategoricalall <- names(v$vars$data_type)[sapply(v$vars$data_type,
                                                            function(x) any(x %in% c("factor", "character", "logical")))]

  #### vars Excluded categorical variables ####
  v$vars$vnexcluded <- names(df0[, sapply(df0,
                                          function(x) length(levels(x))) > max_levels_cat_var]
  )

  #### vars Maintained categorical variables ####
  v$vars$vncategorical <- names(df0[v$vars$vncategoricalall][, sapply(df0[v$vars$vncategoricalall],
                                                                      function(x) length(levels(x))) <= max_levels_cat_var]
  )

  #### vars How many variables of each type ####
  v$vars$hmvars <- length(df0)
  v$vars$hmnumeric <- length(v$vars$vnnumeric)
  v$vars$hmcategoricalall <- length(v$vars$vncategoricalall)
  v$vars$hmexcluded <- length(v$vars$vnexcluded)
  v$vars$hmcategorical <- length(v$vars$vncategorical)
  v$vars$hmtarget <- length(v$vars$vntarget)

  ##### ini title ####
  v$ini$title <- paste("General characteristics of", df_name, "data frame")

  #### ini Table features ####
  v$ini$captables$Tfea <- paste0('The data frame ', df_name,
                                 ' had the following dimensions.')
  v$ini$tables$Tfea <- data.frame(Features = c("Number of observations",
                                               "Number of variables",
                                               "Number of numerical variables",
                                               "Number of categorical analyzed",
                                               "Number of categorical excluded"),
                                  Result = c(v$vars$hmobservations,
                                             v$vars$hmvars,
                                             v$vars$hmnumeric,
                                             v$vars$hmcategorical,
                                             v$vars$hmexcluded
                                  )
  )

  #### ini Table for numeric ####
  if (v$vars$hmnumeric > 0) {
    v$ini$captables$Tnum <- paste0("Numeric variables for ",
                                   df_name, " data frame.")
    v$ini$tables$Tnum <- agtablenumeric(df0, v$vars$vnnumeric)
  }
  #### ini Table for categorical ####
  v$ini$captables$Tcat <- paste0("Categorical variables for ", df_name, " data frame.")
  v$ini$tables$Tcat <- agtablecategorical(df0,
                                          cat_var = v$vars$vncategoricalall,
                                          excluded = v$vars$vnexcluded)


  #### ini Explanatory text ####
  ### Text if there are excluded variables
  Textexcluded <- ifelse(v$vars$hmexcluded > 0,
                         paste0("Other ", v$vars$hmexcluded,
                                " categorical variables were excluded ",
                                "because they had more than ",max_levels_cat_var,
                                " levels: ",
                                agvectorastext(v$vars$vnexcluded), "."),
                         "")

  ### Final text
  v$ini$texts$explanation <- paste0("The data frame has ", v$vars$hmvars,
                                    " variables.", " From these, ",
                                    v$vars$hmnumeric,  " are numeric and ",
                                    v$vars$hmcategorical, " are categorical.  ",
                                    Textexcluded)

  rm(Textexcluded)

  #### ini Clean data frame ####

  ### Exclude excluded categorical (too many levels)
  v$cleandata <- df0[, sapply(df0,
                              function(x) length(levels(x))) <= max_levels_cat_var]

  ### Exclude observations with NA in target_var
  if (length(target_var) == 1) {
    v$cleandata <- v$cleandata[!is.na(v$cleandata[[target_var]]),]
  }
  if (length(target_var) > 1) {
    isna <- is.na(v$cleandata[[target_var[1]]])
    # i = target_var[-1]
    for (i in target_var[-1]) {
      isna <- isna * is.na(v$cleandata[[i]])
    }
    v$cleandata <- v$cleandata[!isna, ]
  }

  #### ini Choose variables if too many (not working, not necessary) ####
  # ### Interesting for ordering the variables from more to less effect
  #
  # if (!is.null(top_k_features)) {
  #   var_imp <- FSelector::chi.squared(target_var ~ ., df0[v$vars$vncategorical])
  #   var_imp <- eval(parse(text = f_screen_model))(substitute(target_var ~ ., list(target_var = as.name(target_var))), df0)
  #   top_k_vars <- cutoff.k(var_imp, top_k_features)
  #   retain_cols <- c(top_k_vars, target_var)
  #   df <- df[, colnames(df) %in% retain_cols]
  # }

  #### return ####
  return(v)
}
### Examples
# v <- agvcharacteristics(iris, "Species")
# v$vars$vncategorical
# v$vars$vntarget
# iris[[v$vars$vntarget]]
# iris[v$vars$vntarget]
#
# v <- agvcharacteristics(iris, c("Species", "Petal.Width"))
# v$vars$vntarget
# iris[[v$vars$vntarget]]  # Not working
# iris[v$vars$vntarget]
# iris[[v$vars$vntarget[1]]]

# res$vars <- agvcharacteristics(df0, target_var)
#_______________________________________________________________________________

