###### Function aggeneratexlsx Creates xlsx from solutions list ##################
### Created: 180308 Author: Alfonso Garmendia  algarsal at upvnet dot upv dot es
###
### V0.2.1 181004: Fix formats titles and subtitles
### v0.2.0 180716: Change res format to res\$hoja\$variable\$plots\$nombre
### v0.1.3 180512: Add roxygen2 documentation.
### V0.1.2 180510: Print also "recordedplot"s
### V0.1.1 180409: Fix bug on columns number for tables and plots.
### V0.1.0 180321: Working version.
###                Might need some poolish when tryed with different data sets.
### V0.0.1: simplified from https://www.r-bloggers.com/automating-basic-eda/
### and    vignette("Introduction", package = "openxlsx")
### and    vignette("formatting", package = "openxlsx")
#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
#### Roxigen ####
#' Generate Excel document from results list.
#'
#' A results list is a list with the results of the analysis in a standarized
#' way. The list has different levels:
#'
#' - level 1: Names for different worksheets. One worksheet for each analysis
#'       type and each dependent variable.
#'
#' - level 2: Names for different Columns in each worksheet. Usually
#' corresponding with each dependent variable. May have "title"
#'       and "subtitle" for each worksheet.
#'
#' - level 3: Has to be one of these:
#'       "title", a character vector with title. will be on first row.
#'       "subtitle", a character vector with subtitle. will be on second row.
#'       "plots", list of plots (mainly from ggplot)
#'       "capplots", list of characters with same names as plots for captions.
#'       "tables", list of data frames. If same name than plots,
#'                 will be considered related.
#'       "captables", list of characters with same name as tables for captions.
#'       "texts", list of characters, with other comments.
#'                If same name than plots or tables will be considered related.
#'
#' - level 4: names of plots, tables or texts. If same name will be considered
#'            related. This is necessary for captions of plots and tables.
#'
#' @param res The results list
#' @param df_name Name of the data frame or project for the titles.
#' @param ... Names needed for next parameters.
#' @param folder Folder where export the Excel file.
#' @param filename Changes the default filename.
#' @param basefile Changes the default basefile.
#' @param min_col_sep Minimum number of worksheet columns between plots or
#'    tables.
#' @param plotwidth Plot width, in inches.
#' @param plotheight Plot height, in inches.
#' @param headerstyle Changes default header style.
#'
#' @return
#' An Excel (.xlsx) file in the folder specified. With title, subtitle, plots
#' and tables for each worksheet. In the order specified in the results list.
#'
#' @import openxlsx
#'
#' @export
#'
#' @examples
#'
#' ### REDO EXAMPLES (OLD) #######################################
#' res <- list()
#' aovhsd <- agaovhsd(Data = iris, x = "Species", y = "Petal.Length",
#'                    nam = "Iris species")
#' res$iris$plots$petlength$barplot <- aovhsd$plots$barplot
#' res$iris$plots$petlength$violin <- aovhsd$plots$violin
#' res$iris$capplots$petlength$barplot <- aovhsd$capplots$barplot
#' res$iris$capplots$petlength$violin <- aovhsd$capplots$violin
#' res$iris$tables$petlength$TA <- aovhsd$tables$TA
#' res$iris$tables$petlength$TB <- aovhsd$tables$TB
#' res$iris$captables$petlength$TA <- aovhsd$captables$TA
#'
#' aggeneratexlsx(res, df_name = "Iris")
#'
#' @details
#'
#' Require openxlsx.
#'
#' @seealso
#' openxlsx, agaddtable
#'
#' @author Alfonso Garmendia (Universitat Politècnica de València)
#'
#### Init ####
# res <-  agautoanalyses(iris)
# df_name = "Selected-dataframe"
# folder = "results"
# filename = NULL
# basefile = NULL
# min_col_sep = 8
# plotwidth = 8
# plotheight = 5
# headerstyle = NULL

# output_file_name = "results"  # still not implemented

#### Function ####
aggeneratexlsx <- function(res, df_name = "dataframe", ... ,
                           folder = "results",
                           filename = NULL, basefile = NULL,
                           min_col_sep = 8, plotwidth = 8, plotheight = 5,
                           headerstyle = NULL) {
  #### requires ####
  # agrequire("openxlsx")

  #### Styles ####

  titlestyle <- createStyle(textDecoration = c("Bold", "underline"),
                            fontColour = "black",
                            fontName = "Times", fontSize = 18,
                            valign = "center")

  subtitlestyle <- createStyle(textDecoration = c("Bold"),
                               fontColour = "black",
                               fontName = "Times", fontSize = 14,
                               valign = "center")

  vartitlestyle <- createStyle(textDecoration = c("Bold"),
                            fontColour = "black",
                            fontName = "Times",
                            valign = "center")

  varsubtitlestyle <- createStyle(fontColour = "black",
                               fontName = "Times",
                               valign = "center")

  textstyle <- createStyle(fontColour = "black",
                           fontName = "Arial", bgFill = "lightgrey",
                           valign = "center", halign = "left")

  #### Create workbook ####
  ### basefile
  if (!is.null(basefile)) { wb <- loadWorkbook(basefile)
  } else {
    ## Create a blank workbook
    wb <- createWorkbook()
  }

  ### Useful names
  useful0 <- c("title", "subtitle")
  useful <- c(useful0,
              "tables", "captables",
              "plots", "capplots",
              "texts")

  #### Create WORKSHEETS, titles and subtitles ####
  # i = names(res)[6]
  rc <- list()
  worksheets <- character(0)
  for (i in names(res)) {           # One worksheet for each list into res
    #### next ####
    ### if not a list
    if (!"list" %in% class(res[[i]])) next               # is not a list

    #### if not useful
    np1 <- any(names(res[[i]]) %in% useful0)
    for (j in names(res[[i]])) {
      np1 <- c(np1, any(names(res[[i]][[j]]) %in% useful))
    } # for each level 2 name
    if (!any(np1)) {
      rm(np1)
      next     # not any useful in the list
    } # if there is useful for worksheet or next
    rm(np1)

    #### Add worksheet ####
    addWorksheet(wb, i)
    pageSetup(wb, i, orientation = "landscape",
              fitToWidth = TRUE, fitToHeight = TRUE)
    worksheets <- c(worksheets, i) # names of worksheets

    #### init rows and columns ####
    rc[[i]]$r <- 3   # row for varstitles or for first plots
    # rc[[i]]$r2 <-                # First row for each column
    # rc[[i]]$rp <- 3              # Row for next plot
    
    rc[[i]]$c <- 3      # first column for plots, lists of plots, legend (-1)
    
    rc[[i]]$plotheight <- round(plotheight * 4.7)   # plot heigth
    rc[[i]]$rl <- rc[[i]]$rp + rc[[i]]$plotheight + 1     # row for plots legends
    # }
    
    ### number of rows for each inch of plot
    # 24/5  # plotheight = 5
    # 28/6  # plotheight = 6
    
    #### Title and subtitle ####
    writeData(wb, sheet = i, x = res[[i]]$title,
              startRow = 1, startCol = 1)

    ### Title format
    conditionalFormatting(wb, i,
                          cols = 1, rows = 1,
                          rule = "$A1 != 0", style = titlestyle)
    setRowHeights(wb, i, rows = 1, heights = 30)

    ### add subtitle
    if("subtitle" %in% names(res[[i]])){
      writeData(wb, sheet = i, x = res[[i]]$subtitle,
                startRow = 2, startCol = 1)
      rc[[i]]$r <- rc[[i]]$r + 1
    }
    ### Subtitle format
    conditionalFormatting(wb, i,
                          cols = 1, rows = 2,
                          rule = "$A1 != 0", style = subtitlestyle)
    setRowHeights(wb, i, rows = 2, heights = 25)
  }   # One worksheet for each list into res

  #### Calculate columns widths ####
  cwm <- min_col_sep               # minimum number of columns for each cv
  ### Tables
  # i <- worksheets[6]
  # j <- names(res[[i]])[1]
  for (i in worksheets) {              # Each Worksheet
    cwsum <- rc[[i]]$c  # SUM of columns for next one. Initialize each worksheet

    for (j in names(res[[i]])) {       # For each dependent variable (columns)
      cwt <-                         #
        cwp <- integer(0)
      #### NEXT ####
      if (!"list" %in% class(res[[i]][[j]])) next
      #### From Tables ####
      if (length(res[[i]][[j]]$tables) > 0) { # If there are tables
        tables <- res[[i]][[j]]$tables

        ### If data frame ###
        if ("data.frame" %in% class(tables)) { # if data frame
          cwt <- c(cwt, length(tables)) # number of variables
        } # if data frame

        ### If list ###
        if ("list" %in% class(tables)) { # If list of tables
          for (k in 1:length(tables)) {
            cwt <- c(cwt, length(tables[[k]]))
          }
        } # If list of tables
      } else {tables <- NULL} # If there are tables
      #### Columns width from plots ####rc[[i]][[j]]$c
      if (length(res[[i]][[j]]$plots) > 0) cwp <- round(plotwidth * 1.6)

      #### Column width and Column number ####
      rc[[i]][[j]]$cw <- max(cwt + 2, cwp, cwm)
      rc[[i]][[j]]$c <- cwsum
      cwsum <- cwsum + rc[[i]][[j]]$cw
    } # For each dependent variable (columns)
  } # Each Worksheet
  rm(cwm, cwp, cwsum, cwt, tables)

  #### Titles and subtitles for variables ####
  # i <- worksheets[6]
  # j <- names(res[[i]])[1]
  for (i in worksheets) {                                     # Each Worksheet
    rc[[i]]$titles <- FALSE
    rc[[i]]$subtitles <- FALSE

    for (j in names(res[[i]])) {       # For each dependent variable (columns)
      #### NEXT ####
      if (!"list" %in% class(res[[i]][[j]])) next

      #### Add Title ####
      if (length(res[[i]][[j]]$title) > 0) { # If there is title
        writeData(wb, sheet = i, x = res[[i]][[j]]$title,
                  startRow = rc[[i]]$r, startCol = rc[[i]][[j]]$c)

        ### Title format ####
        conditionalFormatting(wb, i,
                              cols = rc[[i]][[j]]$c, rows = rc[[i]]$r,
                              rule = "$A1 != 0", style = vartitlestyle)
        # setRowHeights(wb, i, rows = rc[[i]]$r, heights = 20)

        ### check titles ####
        rc[[i]]$titles <- TRUE
        
      } # If there is title

      ### add subtitle ####
      if (length(res[[i]][[j]]$subtitle) > 0) { # If there is subtitle
        writeData(wb, sheet = i, x = res[[i]][[j]]$subtitle,
                  startRow = rc[[i]]$r + 1, startCol = rc[[i]][[j]]$c)

        ### Subtitle format ####
        conditionalFormatting(wb, i,
                              cols = rc[[i]][[j]]$c, rows = rc[[i]]$r + 1,
                              rule = "$A1 != 0", style = varsubtitlestyle)
        # setRowHeights(wb, i, rows = rc[[i]]$r + 1, heights = 18)

        ### check subtitles ####
        rc[[i]]$subtitles <- TRUE
        
      } # If there is subtitle
     } # For each dependent variable (columns)

      #### ADD ROWS ####
    if (rc[[i]]$titles) rc[[i]]$r <- rc[[i]]$r + 1
    if (rc[[i]]$subtitles) rc[[i]]$r <- rc[[i]]$r + 1
    ### Margin
    if (rc[[i]]$titles || rc[[i]]$subtitles) rc[[i]]$r <- rc[[i]]$r + 1
    
  } # Each Worksheet

  #### PLOTS ####
  # i <- worksheets[4]
  # j <- names(res[[i]])[1]
  for (i in worksheets) {                                     # Each Worksheet
    rowsplots <- rc[[i]]$r              # record the rows for each colummn
    for (j in names(res[[i]])) {       # For each dependent variable (columns)
      #### next ####
      ### if not a list
      if (!"list" %in% class(res[[i]][[j]])) next           #if is not a list

      #### if not plots into list
      # np1 <- any(names(res[[i]][[j]]) %in% useful)
      np1 <- "plots" %in% names(res[[i]][[j]])
      if (!np1) {
        rm(np1)
        next     # not any plot in the list
      }
      rm(np1)

      #### initialize rows before start each column ####
      ph <- rc[[i]]$plotheight
      rc[[i]][[j]]$r <- rc[[i]]$r
      rc[[i]][[j]]$rl <- rc[[i]][[j]]$r + ph + 1        # row for plot legends

      #### PLOTS ####
       if (length(res[[i]][[j]]$plots) > 0) {   # if something in "plots"
        #### IF ONE PLOT without name ####
        if (any(class(res[[i]][[j]]$plots) %in% c("gg", "ggplot", "plot",
                                                  "recordedplot"))) {
          #### Plot ####
          print(res[[i]][[j]]$plots)
          insertPlot(wb, i, width = plotwidth, height = plotheight,
                     startRow = rc[[i]][[j]]$r, startCol = rc[[i]][[j]]$c)

          #### legends ####
          writeData(wb, i, x = res[[i]][[j]]$capplots,
                    startRow = rc[[i]][[j]]$rl,
                    startCol = rc[[i]][[j]]$c - 1)

          #### new row ####
          rc[[i]][[j]]$r <- rc[[i]][[j]]$rl + 2
          rowsplots <- c(rowsplots, rc[[i]][[j]]$r)
        } # if plots is one plot

        #### IF LIST OF PLOTS (with names for capplots) ####
        if ("list" %in% class(res[[i]][[j]]$plots)) {    # if plots is a list
          plotnames <- names(res[[i]][[j]]$plots)
          # p <- plotnames[1]
          for (p in plotnames) {                 # for names of plots in list
            #### Next ####
            ### if not a plot
            if (!any(class(res[[i]][[j]]$plots[[p]]) %in%
                     c("gg", "ggplot", "plot", "recordedplot"))) next
            ### if error
            if (inherits(try(print(res[[i]][[j]]$plots[[p]]), silent = TRUE),
                         "try-error")) next

            #### Print plot into worksheet ####
            print(res[[i]][[j]]$plots[[p]])
            insertPlot(wb, i, width = plotwidth, height = plotheight,
                       startRow = rc[[i]][[j]]$r, startCol = rc[[i]][[j]]$c)
            #### legends ####
            writeData(wb, i, x = res[[i]][[j]]$capplots[[p]],
                      startRow = rc[[i]][[j]]$rl,
                      startCol = rc[[i]][[j]]$c - 1)
            #### rows ####
            rc[[i]][[j]]$r <- rc[[i]][[j]]$rl + 2         # row for next plot
            rc[[i]][[j]]$rl <- rc[[i]][[j]]$r + ph + 1    # row for next legend
            rowsplots <- c(rowsplots, rc[[i]][[j]]$r)


          } # for p in plotnames (names of plots in list)
        }  # if plots is a list of plots
      }   # if something in "plots"
    }    # For each dependent variable (columns)

    #### new row for each worksheet ####
    rc[[i]]$r <- max(rowsplots)

  }    # For each Worksheet

  #### TABLES ####
  # i <- worksheets[6]
  # j <- names(res[[i]])[1]
  for (i in worksheets) {              # Each Worksheet
    rowstables <- rc[[i]]$r            # record the rows for each colummn
    for (j in names(res[[i]])) {       # For each dependent variable (columns)
      #### next ####
      ### if not a list
      if (!"list" %in% class(res[[i]][[j]])) next           #if is not a list

      #### if not tables into list
      # np1 <- any(names(res[[i]][[j]]) %in% useful)
      np1 <- "tables" %in% names(res[[i]][[j]])
      if (!np1) {
        rm(np1)
        next     # not any plot in the list
      }
      rm(np1)

      #### if empty "tables"
      if (length(res[[i]][[j]]$tables) < 1) next

      #### initialize rows before start each column ####
      rc[[i]][[j]]$rl <- rc[[i]]$r     # row for tables legends
      rc[[i]][[j]]$r <- rc[[i]][[j]]$rl + 1

      #### IF 1 TABLE ####
      if (class(res[[i]][[j]]$tables) == "data.frame") {
        agaddtable(wb, sheet = i, cap = res[[i]][[j]]$captables,
                   tab = res[[i]][[j]]$tables, name = j,
                   startrow = rc[[i]][[j]]$r, startcol = rc[[i]][[j]]$c - 1)
        #### New row
        rc[[i]][[j]]$r <- rc[[i]][[j]]$r + length(res[[i]][[j]]$tables[[1]]) + 3
        rowstables <- c(rowstables, rc[[i]][[j]]$r)
      }

      #### IF LIST OF TABLES ####
      if (class(res[[i]][[j]]$tables) == "list") { # if tables is list
        tables <- names(res[[i]][[j]]$tables)
        # && class(res[[i]][[j]]$tables[[1]]) == "data.frame")
        #### Para cada nombre de tabla ####
        # l = tables[1]
        for (l in tables) { # For each table name
          #### Next ####
          ### if not data frame
          if (!"data.frame" %in% class(res[[i]][[j]]$tables[[l]])) next

          #### Table ####
          captable <- res[[i]][[j]]$captables[[l]]
          tab <- res[[i]][[j]]$tables[[l]]

          agaddtable(wb, sheet = i, cap = captable,
                     tab = tab, name = j,
                     startrow = rc[[i]][[j]]$r, startcol = rc[[i]][[j]]$c - 1)

          #### Next row ####
          rc[[i]][[j]]$r <- rc[[i]][[j]]$r + length(tab[[1]]) + 3
          rowstables <- c(rowstables, rc[[i]][[j]]$r)
        } # For each table name
      }  # if tables is list
    }   # For each dependent variable (columns)

    #### Row for text ####
    rc[[i]]$r <- max(rowstables)

  }           # Each Worksheet

  #### TEXTS ####
  ### Second level texts
  # i <- worksheets[3]
  # j <- names(res[[i]])[6]
  for (i in worksheets) {              # Each Worksheet
      #### next ####
      ### if not a list
      if (!"list" %in% class(res[[i]])) next           #if is not a list
      
      #### if not texts into list
      # np1 <- any(names(res[[i]][[j]]) %in% useful)
      np1 <- "texts" %in% names(res[[i]])
      if (!np1) {
        rm(np1)
        next     # not any text in the list
      }
      rm(np1)
      
      #### if empty "text"
      if (length(res[[i]]$texts) < 1) next
      
      #### IF 1 TEXT ####
      if ("character" %in% class(res[[i]]$texts)) { # If texts "character"
        ### Write text ####
        writeData(wb, i, x = res[[i]]$texts,
                  startRow = rc[[i]]$r, startCol = 1)
        
        ### Text format ####
        nlines <- as.numeric(summary(gregexpr("\n", res[[i]]$texts))[1])
        h <- (1 + nlines) * 20  # Height of the text line
        
        conditionalFormatting(wb, i,
                              cols = 1:30, rows = rc[[i]]$r,
                              rule = "$A1 != 0", style = textstyle)
        
        setRowHeights(wb, i, rows = rc[[i]]$r, heights = h)
        
        rm(nlines, h)
        
        ### Add row ####
        rc[[i]]$r <- rc[[i]]$r + 2
        
      } # If texts "character"
      #### IF LIST OF TEXTS ####
      if ("list" %in% class(res[[i]]$texts)) { # If texts "list"
        texts <- names(res[[i]]$texts)
        # p <- texts[1]
        for (p in texts) { # for each texts names
          ### Write text ####
          writeData(wb, i, x = res[[i]]$texts[[p]],
                    startRow = rc[[i]]$r, startCol = 1)
          
          ### Text format ####
          nlines <- as.numeric(summary(gregexpr("\n", res[[i]]$texts[[p]]))[1])
          h <- (1 + nlines) * 20  # Height of the text line
          
          conditionalFormatting(wb, i,
                                cols = 1:30, rows = rc[[i]]$r,
                                rule = "$A1 != 0", style = textstyle)
          
          setRowHeights(wb, i, rows = rc[[i]]$r, heights = h)
          
          rm(nlines, h)
          
          ### Add row ####
          rc[[i]]$r <- rc[[i]]$r + 2
          
        } # for each texts names
      } # If texts "list"
  } # Each Worksheet
  
  ### Third level texts
  # i <- worksheets[3]
  # j <- names(res[[i]])[6]
  for (i in worksheets) {              # Each Worksheet
    for (j in names(res[[i]])) {       # For each dependent variable (columns)
      #### next ####
      ### if not a list
      if (!"list" %in% class(res[[i]][[j]])) next           #if is not a list

      #### if not texts into list
      # np1 <- any(names(res[[i]][[j]]) %in% useful)
      np1 <- "texts" %in% names(res[[i]][[j]])
      if (!np1) {
        rm(np1)
        next     # not any text in the list
      }
      rm(np1)

      #### if empty "text"
      if (length(res[[i]][[j]]$texts) < 1) next

      #### IF 1 TEXT ####
      if ("character" %in% class(res[[i]][[j]]$texts)) { # If texts "character"
        ### Write text ####
        writeData(wb, i, x = res[[i]][[j]]$texts,
                  startRow = rc[[i]]$r, startCol = 1)

        ### Text format ####
        nlines <- as.numeric(summary(gregexpr("\n", res[[i]][[j]]$texts))[1])
        h <- (1 + nlines) * 20  # Height of the text line

        conditionalFormatting(wb, i,
                              cols = 1:30, rows = rc[[i]]$r,
                              rule = "$A1 != 0", style = textstyle)

        setRowHeights(wb, i, rows = rc[[i]]$r, heights = h)

        rm(nlines, h)

        ### Add row ####
        rc[[i]]$r <- rc[[i]]$r + 2

      } # If texts "character"
      #### IF LIST OF TEXTS ####
      if ("list" %in% class(res[[i]][[j]]$texts)) { # If texts "list"
        texts <- names(res[[i]][[j]]$texts)
        # p <- texts[1]
        for (p in texts) { # for each texts names
          ### Write text ####
          writeData(wb, i, x = res[[i]][[j]]$texts[[p]],
                    startRow = rc[[i]]$r, startCol = 1)

          ### Text format ####
          nlines <- as.numeric(summary(gregexpr("\n", res[[i]][[j]]$texts[[p]]))[1])
          h <- (1 + nlines) * 20  # Height of the text line

          conditionalFormatting(wb, i,
                                cols = 1:30, rows = rc[[i]]$r,
                                rule = "$A1 != 0", style = textstyle)

          setRowHeights(wb, i, rows = rc[[i]]$r, heights = h)

          rm(nlines, h)

          ### Add row ####
          rc[[i]]$r <- rc[[i]]$r + 2

        } # for each texts names
      } # If texts "list"
    } # For each dependent variable (columns)
  } # Each Worksheet

  #### RETURN ####
    if (is.null(filename)) filename <- paste0(format(Sys.time(), "%y%m%d"),
                                              "-Results-", df_name, ".xlsx")

    output_file_name <- paste0(folder, "/", filename)

    saveWorkbook(wb, file = output_file_name, overwrite = TRUE)

    message(paste("The file", output_file_name, "was created."))
    # openXL(wb)
    # return(wb)
  } # Function end


###### Function agaddtable adds table to existing work book ####################
### Created: 180416 Author: Alfonso Garmendia  algarsal at upvnet dot upv dot es

### V0.0.1: working. Adds table to existing work book for Excel
#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

agaddtable <- function(wb, sheet, cap, tab, name, ...,
                       startrow = 1, startcol = 1,
                       headerstyle = headerstyle) {
  #### Zebra style ####
  zebrastyle <- function(wb, sheet, cols, rows){
    negStyle <- createStyle(bgFill = '#eeeeee')
    posStyle <- createStyle(bgFill = "white")
    conditionalFormatting(wb, sheet, cols = cols, rows = rows,
                          rule = "=ISEVEN(ROW())", style = negStyle)
    conditionalFormatting(wb, sheet, cols = cols, rows = rows,
                          rule = "=ISODD(ROW())", style = posStyle)

  }

  #### Last row style ####
  laststyle <- createStyle(border = "top", borderColour = "black",
                           borderStyle = "medium")

  #### Header style ####
  hs1 <- createStyle(fgFill = "white", halign = "CENTER",
                     textDecoration = "Bold", fontColour = "black",
                     border = c("top", "bottom"), borderStyle = c("medium", "thin"))

  ### Blue header
  # hs1 <- createStyle(fgFill = "#4F81BD", halign = "CENTER",
  #                    textDecoration = "Bold",
  #                    border = "bottom", fontColour = "white")


  #### Legends ####
  writeData(wb, sheet, x = cap,
            startRow = startrow, startCol = startcol)

  #### Tables ####
  writeData(wb, sheet, x = tab,
            startRow = startrow + 1, startCol = startcol + 1,
            headerStyle = hs1)

  #### Format tables ####
  ### format zebra
  lastcol <- startcol + length(tab)
  lastrow <- startrow  + length(tab[[1]]) + 1

  zebrastyle(wb, sheet, (startcol + 1):lastcol, (startrow + 2):lastrow)

  #### Tables Columns width automatic (no funciona)
  # setColWidths(wb, i, cols = rc[[i]]$c:lastcol, widths = "auto")
  options("openxlsx.minWidth" = 8.5)
  setColWidths(wb, sheet, cols = (startcol + 1):lastcol, widths = "auto")

  ### Last row with black line
  conditionalFormatting(wb, sheet,
                        cols = (startcol + 1):lastcol, rows = lastrow + 1,
                        rule = "$A1 != 0", style = laststyle)
  ##

}

### Example ####
# agaddtable(wb, sheet = i, cap = res[[i]]$captables[[j]],
#            tab = res[[i]]$tables[[j]], name = j,
#            startrow = rc[[i]]$r, startcol = rc[[i]]$c - 1)
