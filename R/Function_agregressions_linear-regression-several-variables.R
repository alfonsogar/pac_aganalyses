###### Function agregressions Several linear regressions #######################
### Created: 180419 Author: Alfonso Garmendia  algarsal at upvnet dot upv dot es

### V0.1.0 180620: Package version. Integrate.
### V0.0.1: Not working yet.
#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

#### Roxigen documentation ####
#' Linear regressions
#'
#' Aggregates into the same list the results of several linear
#' regressions. Each of them with plot and captions from
#' \code{\link{agregressionbi}}.
#'
#' @param df0 A data frame
#' @param x Number pointing to the x variable inside "df0"
#'   dataframe. Should be numeric.
#' @param y Number pointing to the y variable inside "df0"
#'   dataframe. Should be numeric.
#' @param ... Names needed for next parameters.
#' @param res List of results to add the results to. Default to NULL (new list).
#' @param equation TRUE/FALSE for including equation in the plot.
#' @param level Confidence levels. Default = 0.95.
#' @param ggtheme Theme for ggplot barplot. Default = ggplot2::theme_bw().
#'
#' @return
#' A list with a sublist for each dependent variable and plots and captions
#' for each independent variable. If a results list is pointed in parameters,
#' it will add the results to this list:
#'
#' - $Regr_dependent$independent$plots
#' - $Regr_dependent$independent$capplots
#'
#' @export
#'
#' @examples
#' A <- agregressions(df0 = iris, x = 1:3, y = 3:4)
#' A
#'
#' @seealso
#' \code{\link{agregressionplot}} for details on the figure and caption and
#' \code{\link{agregressionbi}} for details of the regression analyses.
#'
#' @author Alfonso Garmendia (Universitat Politècnica de València)
#'
##### Init function ####
# df0 <- iris
# x <- 1:3
# y <- 3:4
# res = res
# equation = TRUE
# level = 0.95
# ggtheme = ggplot2::theme_bw()

#### Function ####
agregressions <- function(df0, x, y, ..., res = NULL, equation = TRUE,
                          level = 0.95,
                         ggtheme = ggplot2::theme_bw()) {
  #### Require ####
  # agrequire(c("ggplot2", "ggpmisc"))
  #
  #### Check data ####
  if (TRUE) {
    #### df0 ####
    df0 <- agcheckdf(df0, dfname = "df0", funame = "agregressions")

    #### level ####
    if (!is.numeric(level)) stop("agregressions(): 'level' must be numeric.")

    #### Variables assignations ####
    x <- agcheckvar(df0 = df0, v = x, funame = "agregressions", vname = "x",
                    dfname = "df0")
    y <- agcheckvar(df0 = df0, v = y, funame = "agregressions", vname = "y",
                    dfname = "df0")

    #### Variables numeric ####
    ### Coerce
    df0[x] <- as.data.frame(lapply(df0[x], as.numeric))
    df0[y] <- as.data.frame(lapply(df0[y], as.numeric))

    ### check
    # i = x[1]
    for (i in x) {
      if (!is.numeric(df0[[i]])) {
        nam <- names(df0[i])
        stop(paste0("agregressions(): 'x' variables must be numeric. '",
                    nam, "' is not numeric."))
      } # if not numeric
    } # for i in x if not numeric, stop.

    for (i in y) {
      if (!is.numeric(df0[[i]])) {
        nam <- names(df0[i])
        stop(paste0("agregressions(): 'y' variables must be numeric. '",
                    nam, "' is not numeric."))
      } # if not numeric
    } # for i in y if not numeric, stop.
  } # All checks

  #### Activate results list ####
  if (is.null(res)) res <- list()

  #### Analyses for each dependent "y" and each dependent x ####
  # i = y[1]
  # j = x[1]
  for (i in y) {
    for (j in x) {
      #### If is the same, next ####
      if (i == j) next

      #### do agregressionbi ####
      r <- agregressionplot(df0 = df0, x = j, y = i, equation = equation,
                     level = level, ggtheme = ggtheme)

      #### Add to list ####
      den <- paste0("Regr_", r$vars[["y"]])
      inn <- r$vars[["x"]]
      res[[den]][[inn]]$plots <- r$plot
      res[[den]][[inn]]$capplots <- r$caption

    } #  j for each x variable
  #### Title ####
  den2 <- r$vars[["y"]]
  xl <- length(x)
  xn <- agvectorastext(names(df0)[x])
  res[[den]]$title <- paste0("Regressions for '",  den2, "'")
  res[[den]]$subtitle <- paste0("Effect of ", xl, " variables on '",  den2, "': ",
                                xn, ".")
  } # i for each y variable
  #### Output ####
  return(res)
  }
