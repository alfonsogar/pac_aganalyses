# Paquete aganalyses
## Descripción
La idea es hacer un paquete de autoanálisis que sea capaz de hacer todos los análisis preliminares dándole una tabla de datos (dataframe). Opcionalmente se le debería poder dar los nombres (o números) de variables independientes , de variables dependientes y covariantes para centrar los análisis. 

### Versiones

### v0.0.1
En 180504 

Incluye funciones previas. Falta aclararme con roxigen y la documentación.

### V0.0.2

#### En 180605 Idea de origen 

- Investigar en el paquete de autoanálisis para enterarme de cómo funciona:
https://www.r-bloggers.com/automating-basic-eda/

- Análisis univariantes: distribuciones más parecidas, histogramas, tablas.

- INCORPORAR TODO LO HECHO EN LIQUENES-BREIJO Y EN CITRICOS-IRENE

- Organizar roxigen descripciones para poder instalar el paquete y utilizar las funciones en los proyectos.


DOCUMENTOS:
Para cada apartado
- title
- subtitle
- plots, con capplots y tablas, captables y textos con el mismo nombre.
- tablas no en plots, con capplots y textos con el mismo nombre.
- Resto de textos

#### Funciones hechas
#### system
- agvectorastext: from vector to text comma separated
- agsistema:  SYSTEM DETAILS

#### ini
- agvcharacteristics: List of variables characteristics on data frame
- agtablenumeric: General characteristics of numeric variables
- agtablecategorical: General characteristics for categorical variables

#### CATEGORICAL
- agunivar_barplot: plot Univariate Categorical (Counts)
- agunivar_categorical: Creates list univar with title

#### NUMERICAL
- agdistrtry: check possible distributions
- aghistogram: plots ggplot histogram with density
- agdistrplot: plot histogram and selected distributions
- agunivarnum_numeric: all univar numerical analyses

#### Documento "res". Modo lista. estructura

Por ahora "res" tiene:

#### data frame

cleandataframe: Dataframe excluding not used variables and observations with
                    NA in target_var (no terminado)

#### vars

  - vars: características del datavframe y sus variables
    - maintitle: title for the begining of document
    - subtitle; subtitle for the begining
    - hmobservations: how many observations in the raw dataframe
    - data_type: class for each variable
    - vnames: all raw variables names
    - vntarget: variable name(s) for the target variable(s)
    - vnnumeric: variable names for numeric variables
    - vncategoricalall: variable names for all cagegorical variables
    - vnexcluded: variables names for excluded categorical variables because
                  they have too many levels
    - vncategorical: variables names for included categorical variables
    - hmvars: how many variables in the raw data frame
    - hmnumeric: how many numerical variables in the raw data frame
    - hmcategoricalall: how many categorical variables
    - hmexcluded: how many excluded categorical variables
    - hmcategorical: how many included categorical variables

#### ini

  - ini:  descripción general del dataframe y sus variables
    - tables: 3
      - fea: features dataframe
      - num: numeric variables
      - cat: categorical variables
    -captables: 3
      - fea:
      - num:
      - cat:
    - texts
      - explanation: final text with explanation of included and excluded vars.
      
#### Categorical

  - Categorical: Characteristics of each categorical variable.
    - title:
    - tables: Count and percent for each category.
    - captables:
    - plots: Barplot of frequencies.
    - capplots:
    - texts: NO
    
#### Numerical
    
  - Numerical: Characteristics of each numerical variable. Histogram
    - title:
    - tables: Fit to different distributions.
    - captables:
    - plots: Histogram, density and estimated distributions.
    - capplots:
    - texts: Explanation of distributions tests.


#### Bivariate

BIVAR FUNCTIONS depnum indcat
- arreglar las captions de anovas y kruskal.
- aganova(df0, dep, ind): anova for 1 var. Plot. Table with posthoc, levene, normality,...
- agtableanovas(df0, dep, indcat): table with anovas and bonferroni
- agkruskal(df0, dep, ind)
- agtablekruskal(df0, dep, indcat)
- agnumcat(df0, dep, indcat): tables anova and kruskal, plots and tables posthoc.

BIVAR FUNCTIONS depnum indnum
- agregres(df0, dep, ind): regresión lineal. (choose best model). Table with models.
- agcorrel(df0, numvar): correlation between all num var
- agnumnum(df0, dep, indnum): arrange all num-num bivar analyses

BIVAR FUNCTIONS depcat indcat
- agchisq(df0, dep, ind):
- agtablechis(df0, dep, indcat):

BIVAR FUNCTIONS depnum indcat


MULTIVAR PRINCIPAL COMPONENTS
MULTIVAR DISCRIMINANT



#### Roxigen documentations for package
agaovhsd, agautoanalysis, agbarplot, agchisqposthoc, agchisquare,
aggeneratexlsx, agrequire, agviolin, aginteractionaov, agbarplotinteraction,
agkruskalfigs, agregression, agvectorastext, agvcharacteristics, agtablenumeric, 
agtablecategorical, agunivar_barplot, agunivar_categorical, agdistrtry, aghistogram, agdistrplot, agsistema, agunivarnum, ...

- Ver cómo añadir los paquetes que quiero que se instalen al instalar el paquete. Hay tres maneras diferentes de hacerlo. En el archivo DESCRIPTION, añadir en:
* Imports: se instala pero no se activa (recomendado si da error)
* Suggests: se instala opcional (recomendado para poder utilizar el paquete sin el)
* Depends: se instala y se activa (puede haber problemas entre paquetes)

También en roxigen, para cada función se puede poner:
@import pkg , para instalar el paquete y activarlo.
@importFrom pkg fun , para sólo importar alguna función del paquete.

### V0.0.3

1. Ver cómo hacer tests con testthat() de forma que se puedan detectar fallos cuando se cambian las funciones. 
1. Separa agvars de agdfindex, desde agvcharacteristics
1. Nuevo formato para agdfindex (ver viñeta: res\$hoja\$variable\$plots\$nombre)
1. tests para agvars y agindex
1. Nuevo formato para agcategorical y tests
1. Nuevo formato para agnumerical y tests
1. Function agcheckdf(). Checks data frames into other functions.
1. Nuevo formato y rehacer funciones regresiones, con tests.
    - Una función para la figura y analisis linear: agregressionplot()
    - Una función para varias bivariantes linear: agregressions()
1. tests para agcheckdf()
1. Cambiar nombre variable plot_theme por ggtheme (sólo para ggplots).
1. Arreglar anovas y kruskal wallis para que funcionen. Pulir Bugs. 
1. Arreglar output xlsx, para que funcione con el nuevo formato de res. Dividir y simplificar la función.
  1. Crear worksheet y hojas
  1. Medir anchuras de cada variable en cada hoja en columnas
  1. titulos
  1. plots
  1. tables
  1. texts
1. en agkruskalfigs() el barplot sale desordenado. Arreglado, creo. 

1. 181025 Crea función agxlsx2csv que exporta un csv de seguridad. 

## Tareas pendientes
1. En agcreatexls, si no hay carpeta "results", crearla. 
1. Terminar de incluir los análisis bivariantes dentro de if(bi) en autoanalyses().

  1. posible bug en agaovhsd la tabla TB no se incluye bien en el xlsx en tablas solas. no se porqué. Hacer pruebas. 
  1. Añadir nombres de filas en TB de agaovhsd. No salen.

  1. Terminar de pulir anovas y kruskal wallis para que funcionen bien.
     - Utilizar las funciones de barplot y violinplot en kruskal y aov
     - Añadir parametros a kruskal, para gráficas.
     - violinplots con las letras de kruskal wallis.
     - añadir texts explicando la selección en cada caso. 
  1. Hacer tests para aov, kruskal y anovas.También para barplot y violinplot.
  1. Completar viñeta para las funciones anteriores.
  1. Revisar las roxigen  de las funciones anteriores.

  1. Arreglar chi-cuadrado para que funcione.
  1. Terminar de pulir chi-cuadrado.
  1. Hacer tests, viñetas y revisar roxigen de chi-cuadrado

  1. Arreglar factorial para que funcione. Crear funciones necesarias. 
  1. Hacer tests, viñetas y revisar roxigen de factorial. 

1. en agcreatexlsx añadir que los data frames se exporten como hojas separads
1. Hacer tests, viñetas y revisar roxigen de xlsx.
1. comprobar fallos formato aggeneratexlsx en tablas y sobre todo en tablas solas.


1. Arreglar output rmd para que funcione.
1. Hacer tests, viñetas y revisar roxigen de rmd.

1. Hacer funciones para las covariantes que se utilizan:
1. Arreglar agautoanalyses para el Analisis covariante anovas + cual.
1. Arreglar agautoanalyses para el Analisis covariante anovas + cuan.
1. Arreglar agautoanalyses para el Analisis covariante regresion + cual.
1. Arreglar agautoanalyses para el Analisis covariante regresion + cuan.
1. Arreglar agautoanalyses para el Analisis covariante chi-cuadrado + cual.
1. Arreglar agautoanalyses para el Analisis covariante chi-cuadrado + cuan.
1. Arreglar agautoanalyses para el Analisis covariante factorial + cual.
1. Arreglar agautoanalyses para el Analisis covariante factorial + cuan.

1. Multivariantes
1. Arreglar multivariate correlaciones.
1. Arreglar multivariate discriminante.
1. Arreglar otros multivariantes.

1. Poner todas las @seealso como agkruskalfigs, pero corrigiendo los links, como en el capítulo de LINKS de:
https://cran.r-project.org/web/packages/roxygen2/vignettes/formatting.html

To other documentation:

    \code{\link{function}}: function in this package
    \code{\link[MASS]{stats}}: function in another package
    \link[=dest]{name}: link to dest, but show name
    \linkS4class{abc}: link to an S4 class

To the web:

    \url{http://rstudio.com}
    \href{http://rstudio.com}{Rstudio}
    \email{hadley@@rstudio.com} (note the doubled @)


1. Hacer tests con testthat() de todas las funciones que falten. 


1. Organizar script de pruebas para ver si funciona todo: 
    - limpiar morralla libraries, etc del script analyses.
    - importar datos adecuadamente

### Para siguientes versiones

1. Añade otras regressiones al análisis bivariante: logarítmica x, logarítmica y, bilogaritmica y alguna forma de ver cual es la mejor. 

1. en res, añadir bibliografía, utilizando citation, en forma de tabla.

1. Ver si es interesante lo que hay aquí:
https://github.com/ekstroem/dataMaid
https://github.com/ekstroem/dataMaid/blob/master/R/makeDataReport.R

## Dónde vive el proyecto  

https://algarsal@bitbucket.org/alfonsogar/aganalyses.git

## Chuleta crear o actualizar paquete

Para actualizar la documentación de las funciones en el paquete:

```{r only for developing the package, echo=FALSE, eval=FALSE}
#### Install functions ####
# dir ("R")
### Source files that start with "^Function_" in /R
for (i in list.files("R",pattern = "^Function_", full.names = T)) source(i)
rm(i)

#### Setup required libraries ####
agrequire(c("agricolae", "car", "plyr", "dplyr", "ggplot2", "ggpmisc", 
            "ggpubr", "openxlsx", "plotrix")
          # , attach = FALSE
          )

#### To update documentation and load the package ####
devtools::document()

#### To run the tests of the package ####
devtools::test()

### See if installed ####
devtools::session_info() # see if installed

```

## Chuleta instalar paquete

Para instalar el paquete you need to run this from the parent working directory that contains the folder. 

```{r}
# if public
devtools::install_bitbucket("alfonsogar/aganalyses", dependencies = TRUE)

# if private
devtools::install_bitbucket("alfonsogar/aganalyses", dependencies = TRUE,
                            auth_user = "algarsal@upv.es", password = "***")

```

Otra forma es primero bajarse la carpeta y luego instalarla:

```{r}
# system("git clone https://algarsal@bitbucket.org/algarsal/aganalyses.git")
# dir("../")
# devtools::install("../agautoanalyses")
devtools::install("../aganalyses", dependencies = TRUE, build_vignettes = TRUE)

devtools::install("../aganalyses") # if there are problems with vignette.

library(aganalyses)
```


