context("agregressionplot() function. Dotplot for bivariate.")
# library(testthat)
library(aganalyses)

data(iris)

v <- agregressionplot(df0 = iris, x = 1, y = 2)

test_that("agregressionplot() output basic", {
  expect_equal(class(v), "list")
  expect_true("ggplot" %in% class(v$plot))
  expect_equal(class(v$caption), "character")
  expect_equal(class(v$lmcoeficients), "numeric")
  expect_equal(class(v$summary), "data.frame")
  expect_equal(class(v$r2), "numeric")
  expect_equal(class(v$pvalue), "numeric")
  expect_equal(class(v$residuals), "numeric")

  expect_equal(length(v$caption), 1)
  expect_equal(length(v$lmcoeficients), 2)
  expect_equal(length(v$summary), 4)
  expect_equal(length(v$r2), 1)
  expect_equal(length(v$pvalue), 1)
  expect_equal(length(v$residuals), 150)

  expect_equal(v$r2, 0.007159294)
  expect_true(abs(v$pvalue - 0.1518983) < 0.001)

  expect_error(agregressionplot(df0 = iris, x = 4:5, y = 2), "must be ONE variable")
  expect_error(agregressionplot(df0 = iris, x = 4, y = 1:2), "must be ONE variable")
  expect_error(agregressionplot(df0 = iris, x = list(), y = 1))

})
