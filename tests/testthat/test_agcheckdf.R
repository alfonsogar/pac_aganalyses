context("Check data frame and ouput errors if necessary")
library(aganalyses)

data(iris)

test_that("agcheckdf basic output", {
  expect_equal(class(agcheckdf(iris)), "data.frame")
  expect_equal(class(agcheckdf(as.list(iris))), "data.frame")
  expect_equal(class(agcheckdf(iris$Sepal.Length)), "data.frame")
  expect_equal(class(agcheckdf(iris[[1]])), "data.frame")
  expect_error(class(agcheckdf(iris[1,1])),
               '"Dataframe_name" must have at least 3 observations')
})
