% Generated by roxygen2: do not edit by hand
% Please edit documentation in
%   R/Function_agcategorical_creates-list-univar-categorical.R
\name{agcategorical}
\alias{agcategorical}
\title{Autoanalysis of categorical variables}
\usage{
agcategorical(df0, ..., cat_var = NULL, maxlevels = 9,
  ggtheme = ggplot2::theme_bw(), title = "Categorical variables",
  subtitle = "Counts and percentages")
}
\arguments{
\item{df0}{A data frame.}

\item{cat_var}{A number or string (or vector with numbers or names) referring
to categorical variable(s) in df0.}

\item{maxlevels}{Maximum number of levels for categorical vars to be
considered. It should be 9 or less. If more, the densities
plots will not have enough colors.}

\item{ggtheme}{The ggplot theme for plots. By default "ggplot2::theme_bw()",
but any ggplot compatible theme should work.
It is only used for ggplot plots, base plots are not affected.}

\item{title}{Title for the results list.}

\item{subtitle}{Subtitle for the results list.}
}
\value{
A list with tables and plots for categorical variables. With captions.
}
\description{
List of tables and plots (with captions) for categorical variables in a data
frame.
}
\examples{
library (agricolae)
data("heterosis")
agcategorical(df0 = heterosis,
              cat_var = c("Place", "Replication", "Treatment", "Factor"))

}
\author{
Alfonso Garmendia (Universitat Politècnica de València)
}
