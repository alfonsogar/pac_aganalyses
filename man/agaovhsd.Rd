% Generated by roxygen2: do not edit by hand
% Please edit documentation in
%   R/Function_agaovhsd_post-hoc-anova-levene-shapiro.R
\name{agaovhsd}
\alias{agaovhsd}
\title{Anova with tuckey post hoc HSD}
\usage{
agaovhsd(df0, x, y, ..., barplot = TRUE, violin = TRUE, xlab0 = NULL,
  ylab0 = NULL, xangle = NULL, xhjust = NULL,
  lettersheightbarplot = mean(df0[[y]])/4,
  lettersheightviolin = max(df0[, y]) + ((max(df0[, y]) - min(df0[,
  y]))/4), ggtheme = ggplot2::theme_bw(), alpha0 = 0.05,
  maxlevels = 9)
}
\arguments{
\item{df0}{The data frame where x and y are.}

\item{x}{String or number pointing to the independent variable inside "df0"
dataframe. Should be a factor variable.}

\item{y}{SString or number pointing to the dependent variable inside "df0"
dataframe. Should be a numeric variable.}

\item{...}{names needed for next parameters.}

\item{barplot}{If barplot is wanted in the output.}

\item{violin}{If violin plot is wanted in the output.}

\item{xlab0}{x variable name for plots and tables. If NULL, the actual
variable name will be used.}

\item{ylab0}{y variable name for plots and tables. If NULL, the actual
variable name will be used.}

\item{xangle}{Changes the angle of text for x axis label in plots.}

\item{xhjust}{Changes the horizontal justification of text for x axis label
in plots.}

\item{lettersheightbarplot}{Changes the letters height in the barplot.
Default = mean(df0[[y]]) / 4}

\item{lettersheightviolin}{Changes the letters height in the violin plot.
Default = max(df0[, y]) + ((max(df0[, y]) - min(df0[, y])) / 4)}

\item{ggtheme}{Theme for ggplot barplot. Default = ggplot2::theme_bw().}

\item{alpha0}{Significance level for post hoc letters.}

\item{maxlevels}{Maximum number of levels for categorical vars to be
considered. It should be 9 or less. If more, the densities
plots will not have enough colors.}
}
\value{
Returns a list with two tables (principal statistics and principal anova statistics) two plots (barplot and violin plot), Tuckey hsd distance and groups and residuals:

 - $plots$barplot The barplot with posthoc letters and error bars.

 - $plots$violin  The violin plot with boxplot inside and posthoc letters.

 - $capplots$barplot Proposed caption for the barplot.

 - $capplots$violin Proposed caption for the violin plot.

 * tables$TA Table of principal statistics: levels, mean, standard deviation,
       , standard error, skewness, kurtosis and shapiro.test for normality.

 - tables$TB Table of anova statistics: df (degrees of freedom), sum sq,
       Mean Sq, F value, P, HSD, eta.sq, Levene  H0= Variances are equal.
       Requires library(cars) and Shapiro-Wilk test for residuals.

 - captables$TA Caption for table A and B. Intended to be together.

 - $hsd Tukey HSD PostHoc test value

 - $groups Groups for Tukey HSD

 - $residuals Residuals from anova. For ulterior analyses or histograms.
}
\description{
Makes analysis of variance and Tuckey Honest Significant Differences Post Hoc.
Makes tables and plots to ilustrate the analysis.
}
\details{
Require plyr and car to be installed (not necessarily activated).

Require agricolae and ggplot2 active.
}
\examples{
res <- list()
aovhsd <- agaovhsd(df0 = iris, x = "Species", y = "Petal.Length",
                   xlab0 = "Iris species")
(res$iris$plots$petlength$barplot <- aovhsd$plots$barplot)
(res$iris$plots$petlength$violin <- aovhsd$plots$violin)
(res$iris$capplots$petlength$barplot <- aovhsd$capplots$barplot)
(res$iris$capplots$petlength$violin <- aovhsd$capplots$violin)
(res$iris$tables$petlength$TA <- aovhsd$tables$TA)
(res$iris$tables$petlength$TB <- aovhsd$tables$TB)
(res$iris$captables$petlength$TA <- aovhsd$captables$TA)

### Tables should be something like this:
# $TA
# Iris species  N  mean HSD        sd         se       skew   kurtosis    Shapiro
# 1       setosa 50 1.462   c 0.1736640 0.02455980  0.1063939  1.0215761 0.05481147
# 2   versicolor 50 4.260   b 0.4699110 0.06645545 -0.6065077  0.0479033 0.15847784
# 3    virginica 50 5.552   a 0.5518947 0.07804970  0.5494446 -0.1537786 0.10977537
#
# $TB
# Analysis of Variance Table
#
# Response: Petal.Length
# Df Sum Sq Mean Sq F value     Pr(>F)     HSD  eta.sq     Levene  Shapiro
# Species     2 437.10 218.551  1180.2 2.8568e-91 0.20378 0.94137 1.2161e-08
# Residuals 147  27.22   0.185                                               0.036764
#
# $hsd
# [1] 0.20378
#
# $groups
# Petal.Length HSD Iris species
# virginica         5.552   a    virginica
# versicolor        4.260   b   versicolor
# setosa            1.462   c       setosa
#
# $residuals

}
\seealso{
The packages plyr, car, agricolae, ggplot are used. Also functions
\code{\link{agbarplot}} and \code{\link{agviolin}}.

\code{\link[car]{leveneTest}}, for Levene test details.

\code{\link[ggpubr]{ggdensity}}, for density plot details.

\code{\link[agricolae]{skewness}}, for skewness measurement details.

\code{\link[agricolae]{kurtosis}}, for kurtosis measurement details.

\code{\link[stats]{shapiro.test}}, for Shapiro-Wilk normality test details.

\code{\link[agricolae]{HSD.test}}, for post-hoc test details.
}
\author{
Alfonso Garmendia (Universitat Politècnica de València)
}
